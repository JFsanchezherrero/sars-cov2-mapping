#! /usr/bin/python

'''
Calculate coverage of ARCTIC primers scheme to detect pool amplification problems
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse
import numpy as np


def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(
        description="Calculate coverage of ARCTIC primers scheme to detect pool amplification problems")
    parser.add_argument("-i", "--input-cov-file",    dest="covfile", metavar=".depth file",          required=True)
    parser.add_argument("-p", "--primer-scheme-bed", dest="scheme",  metavar="nCoV-2019.scheme.bed", required=True)
    parser.add_argument("-o", "--output-file",       dest="outfile")
    parser.add_argument("-s", "--sample",            dest="sname", default="Sample")
    parser.add_argument("-r", "--runid",             dest="runid", default="RUNID")
    args = parser.parse_args()

    return args

def parse_primer_scheme(args):
    '''
    Parse bed file of ARTCTIC primer scheme to return primer id
    and amplified region (e,.g)

    (primer_id, start, end) --> (1, 30, 410), (2, 320, 726)

    Take into account that _alt primers exist. Get coordinates of the
    longest amplicon.

    Will do with a dictionary, to store all positions related to a primer
    since at the end the first and last coordinate will give the bigger amplicon

    (e,g)

    { 7 : (1875, 1897, 1868, 1890, 2247, 2269, 2242, 2264) }
     Amplicon of primer 7 would be 1868 - 2269
    '''

    with open(args.scheme) as infile:

        amplicons = {}
        for line in infile:
            line = line.rstrip().split("\t")
            # Parse primers that are in the form nCoV-2019_1_LEFT, nCoV-2019_7_RIGHT_alt5
            chrom, start, end, primer, qual, strand = line
            primer = primer.split("_")
            primer_id = int(primer[1])
            if not primer_id in amplicons:
                amplicons[primer_id] = []
            amplicons[primer_id].append(int(start))
            amplicons[primer_id].append(int(end))

        for primer_id in amplicons:
            coords = amplicons[primer_id]
            coords.sort()
            start = coords[0]
            end = coords[-1]
            amplicons[primer_id] = (start, end)

    return amplicons

def parse_samtools_depth(args):
    '''
    Parses the samtools depth file and also warns if that file
    is empty like in the case of negative samples
    '''    

    with open(args.covfile) as infile:
        position_depth = [ int(line.rstrip().split()[2]) for line in infile ]

    if len(position_depth) < 1:
        sys.stderr.write("Empty coverage file\n".format(args.sname))
        with open(args.outfile, "w") as outfile:
            outfile.write("")
        sys.exit(1)

    return position_depth

def calc_amplicon_coverage(args):
    '''
    Given a set of amplicons (with primer id and startd and end positions)
    and a coverage file, calc mean and median coverage for each one and return
    in a dictionary
    '''

    amplicons = parse_primer_scheme(args)
    pos_depth = parse_samtools_depth(args)
    amplicon_coverage = {}

    for amplicon in amplicons:
        start, end = amplicons[amplicon]
        cov_range = pos_depth[start-1: end] # start-1 because 0-based Python coords
        median = np.median(cov_range)
        mean = np.mean(cov_range)
        amplicon_coverage[amplicon] = (median, mean)

    return amplicon_coverage, amplicons

def calc_pool_coverage(args):
    '''
    Given the coverage calculate per amplicon, calculate median and mean
    coverage of odd and even amplicons as odd amplicons are in the amplification
    pool 1 and even amplicons in the amplification pool 2. This will allow to
    detect if anything wrong is going on with any amplification pool
    '''    

    amplicon_coverage, amplicons = calc_amplicon_coverage(args)
    pools = {
    "even_covs_median" : [],
    "odd_covs_median" : [],
    "even_covs_mean" : [],
    "odd_covs_mean" : []
    }
    for amplicon in amplicon_coverage:
        median, mean = amplicon_coverage[amplicon]
        if amplicon % 2 == 0:
            pools["even_covs_median"].append(median)
            pools["even_covs_mean"].append(mean)
        else:
            pools["odd_covs_median"].append(median)
            pools["odd_covs_mean"].append(mean)

    pools["even_covs_median"] = np.median(pools["even_covs_median"])
    pools["even_covs_mean"] = np.mean(pools["even_covs_mean"])
    pools["odd_covs_median"] = np.median(pools["odd_covs_median"])
    pools["odd_covs_mean"] = np.mean(pools["odd_covs_mean"])

    return pools, amplicon_coverage, amplicons

def write_amplicon_coverage(args):
    '''
    Get coverage calculation for amplicons and amplification pools and write them to a file
    '''
    pools, amplicon_coverage, amplicons = calc_pool_coverage(args)
    with open(args.outfile, "w") as outfile:
        outfile.write("Sample;RunID;Amplicon;Pool;Start;End;Measure;Amplicon_Value;Overall_Value\n")
        # Now write fore each amplicon
        for amplicon in amplicon_coverage:
            if amplicon % 2 != 0:
                pool = "Pool_1"
                overall_median = pools["odd_covs_median"]
                overall_mean = pools["odd_covs_mean"]
            else:
                pool = "Pool_2"
                overall_median = pools["even_covs_median"]
                overall_mean = pools["even_covs_mean"]
            median, mean = amplicon_coverage[amplicon]
            start, end = amplicons[amplicon]
            outfile.write("{};{};{};{};{};{};median;{};{}\n".format(
            args.sname,
            args.runid,
            amplicon,
            pool,
            start,
            end,
            median,
            overall_median
            ))
            outfile.write("{};{};{};{};{};{};mean;{};{}\n".format(
            args.sname,
            args.runid,
            amplicon,
            pool,
            start,
            end,
            mean,
            overall_mean
            ))

    return 0


def main():

    args = parse_args()
    write_amplicon_coverage(args)
    return 0


if __name__ == '__main__':
    main()
