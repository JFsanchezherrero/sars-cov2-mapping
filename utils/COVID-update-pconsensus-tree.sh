#!/usr/bin/sh
date=$(date +%y%m%d)
cd /data2/COVID19/analysis/general_results/Pseudoconsensus
# There is a problem with genomes that have been resequenced.
# By some reason they also resequenced samples that already had 90%
# coverage and good QC so they appear as duplicated genomes in the 
# pseudoconsensus alignment and iqtree stops. The workaround I am
# implementing is genreate a list of COVS -> Pseudoconsensus paths.
# If a COV is already in the list, I will keep the pseudoconsensus fasta
# with a more recent timestamp as this should be always the resequenced one 
echo "COV;Pseudoconsensus_path" > COV2Pconsensus.list
# Store old trees in an "old" folder
mv *.treefile old/
# Clean dir from previous runs
rm ./Pseudoconsensus*
rm ./ALL_pseudoconsensus*
# Concatenate all pseudoconsensus from available runs that pass 90% coverage
cat wuhan_strain.fasta > ALL_pseudoconsensus_${date}.fasta
cd /data2/COVID19/analysis/
ls | grep -v "general_results" | while read user
  do
    cd ${user}/illumina/
    ls | while read RUNID
      do
        # Read QC summaries so only samples with >90% are selected
        tail -n +2 ${RUNID}/reports/QC_summary.${RUNID}.txt |
         grep "^COV" | # to avoid negative controls etc
         # And select genomes with more than 90% genomic coverage 
         awk -v FS=";" '$5 >= 0.9' | 
           cut -f1 -d';' | while read COV
           do
             # Search that COV in the list we have created 
             PCPATH=`grep ${COV} /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.list`
             if [ ${?} != 0 ]; then # If not found, add that cov and Pconsensus path to the list
               echo "${COV};/data2/COVID19/analysis/${user}/illumina/${RUNID}/pseudoconsensus/${COV}.pseudoconsensus.fasta" >> \
                     /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.list
             # If that COV is already in the list, take the path to the pseudoconsensus file and calculate the timestamp
             else
                PCONSENSUS=`echo ${PCPATH} | cut -f2 -d';'` 
                tstamp1=`stat -c %Y ${PCONSENSUS}`
                tstamp2=`stat -c %Y /data2/COVID19/analysis/${user}/illumina/${RUNID}/pseudoconsensus/${COV}.pseudoconsensus.fasta`
                # If the timestamp of the new pseudoconsensus file found is newer, remove old COV entry and add this new one
                if [ "${tstamp2}" -gt "${tstamp1}" ]; then 
                    grep -v ${COV} /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.list > \
                                   /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.tmp
                    mv /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.tmp \
                       /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.list
                    echo "${COV};/data2/COVID19/analysis/${user}/illumina/${RUNID}/pseudoconsensus/${COV}.pseudoconsensus.fasta" >> \
                     /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.list
                fi
             fi
           done
      done
    cd /data2/COVID19/analysis/
  done

# We have now generated a list with all COVS and their most recent pseudoconsensus file. Now
# Concatenate this files ina  single multifasta to run iqtree 
cd /data2/COVID19/analysis/general_results/Pseudoconsensus/
sed 's/;/ /' COV2Pconsensus.list | tail -n +2 | while read COV PCPATH 
  do 
    cat ${PCPATH}  >> ALL_pseudoconsensus_${date}.fasta
  done 
  
# Generate the tree with iqtree
/data/Software/iqtree-1.6.10-Linux/bin/iqtree \
    -s ALL_pseudoconsensus_${date}.fasta \
    -m HKY -pre Pseudoconsensus_${date} -nt 12

# Generate the tee with iqtree (GTR model + bootstrap)
#/data/Software/iqtree-1.6.10-Linux/bin/iqtree \
#    -s ALL_pseudoconsensus_${date}.fasta \
#    -m GTR -pre Pseudoconsensus_${date} -nt 12 -bb 1000

# Rename the .treefile extension to .nwk (same results, different extension)
mv Pseudoconsensus_${date}.treefile Pseudoconsensus_${date}.nwk
