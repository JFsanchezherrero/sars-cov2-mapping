#!/usr/bin/python
'''
Given a file with reads identifiers (one each row), 
pick/discard those reads from fasta/fastq file
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import argparse


def parse_args():
    '''
    Parse arguments given to script
    '''
    parser = argparse.ArgumentParser(description="Given a file with reads identifiers (one "\
         "each row), pick/discard those reads from fasta/fastq file")
    parser.add_argument("-in", "--inputf",    dest="inputf",     required=True)
    parser.add_argument("-r", "--readlist",   dest="readlist",   required=True)
    parser.add_argument("-a", "--action",     dest="action",     default="pick", choices={"pick", "discard"})
    parser.add_argument("-o", "--output",     dest="output",     default="stdout_1234567890")
    parser.add_argument("-f", "--format",     dest="fformat",    default="fasta", choices={"fasta", "fastq"})
    parser.add_argument("-c", "--compressed", dest="compressed", action="store_true")
    args = parser.parse_args()

    return args


def ParseReadList(readlist):
    ''' 
    Returns a set with all read descriptions/ids
    '''
    with open(readlist) as infile:
        ids = set(line.rstrip() for line in infile)

    return ids


def PickReads(inputf, fformat, readlist, output, compressed):
    '''
    Write reads in readlist to output
    '''
    from Bio import SeqIO
    if compressed:
        import gzip
        infile = gzip.open(inputf)
    else:
        infile = open(inputf)

    if output == "stdout_1234567890":
        import sys
        outfh = sys.stdout
    else:
        outfh = open(output, "w")

    ids = ParseReadList(readlist)
    for record in SeqIO.parse(infile, fformat):
        read_id = record.description.split()[0]
        if read_id in ids:
            SeqIO.write(record, outfh, fformat)

    infile.close()
    outfh.close()


def DiscardReads(inputf, fformat, readlist, output, compressed):
    '''
    Write reads not in readlist to output
    '''
    from Bio import SeqIO
    if compressed:
        import gzip
        infile = gzip.open(inputf, "rt")
    else:
        infile = open(inputf, "rt")

    if output == "stdout_1234567890":
        import sys
        outfh = sys.stdout
    else:
        outfh = open(output, "w")

    ids = ParseReadList(readlist)
    for record in SeqIO.parse(infile, fformat):
        read_id = record.description.split()[0]
        if read_id not in ids:
            SeqIO.write(record, outfh, fformat)

    infile.close()
    outfh.close()


def main():

    args = parse_args()
    if args.action == "pick":
        PickReads(args.inputf, args.fformat, args.readlist, args.output, args.compressed)
    else:
        DiscardReads(args.inputf, args.fformat, args.readlist, args.output, args.compressed)


if __name__ == "__main__":
    main()
