#!/usr/bin/sh
date=$(date +%y%m%d)
cd /data2/COVID19/analysis/
# GENERATE the HEADER for the MICROREACT CSV
echo "ID;coverage;median_depth;lineage__autocolour;Hospital;latitude;"\
"longitude;year;month;day;epi_week;Gender__autocolour;Age;Residence_city" > \
general_results/MICROREACT/MICROREACT_metadata_${date}.csv
# Add whuan strain line
cat general_results/MICROREACT/wuhan_line.csv >> \
general_results/MICROREACT/MICROREACT_metadata_${date}.csv

ls | grep -v "general_results" | while read user
  do
    cd ${user}/illumina/
    ls | while read RUNID
      do
        # FIRST update metadata files, maybe lineages have changed for example
        COVID-analysis-metadata.py  \
        -m /data2/COVID19/sequencing_data/${user}/illumina/${RUNID}/master_table_*.txt \
        --run-id ${RUNID} --run-directory ${RUNID}/ -o ${RUNID}/metadata
  done
  cd /data2/COVID19/analysis/
done

# Now we retrieve the CSV lines one by one for each COV from the COV2Pconsensus.list
# in /data2/COVID19/analysis/general_results/Pseudoconsensus/
# In this way we can be sure there are no duplicates lines for resequenced samples
# and we take the rigth line
cd /data2/COVID19/analysis/
tail -n +2 /data2/COVID19/analysis/general_results/Pseudoconsensus/COV2Pconsensus.list |
  sed 's/;/ /' | while read COV PCPATH
    do
      MDATA_PATH=`echo ${PCPATH} | cut -f1-7 -d'/'`
      MDATA_PATH="${MDATA_PATH}/metadata"
      grep -w ${COV} ${MDATA_PATH}/MICROREACT_*_metadata.csv
    done >> general_results/MICROREACT/MICROREACT_metadata_${date}.csv
