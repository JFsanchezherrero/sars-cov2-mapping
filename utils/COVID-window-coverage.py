#!/usr/bin/python

'''
Calculate mean and median coverage for COVID19 samples across
windows, annotated genes and ARTIC primers
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse
import numpy as np


SCov2_annotation = {
# "SCov2_genome"    : [1, 29903],
"five_prime_UTR"  : [    1,   265],
"ORF1ab"          : [  266, 21555],
"gene_S"          : [21563, 25384],
"ORF3a"           : [25393, 26220],
"gene_E"          : [26245, 26472],
"gene_M"          : [26523, 27191],
"ORF6"            : [27202, 27387],
"ORF7a"           : [27394, 27759],
"ORF8"            : [27894, 28259],
"gene_N"          : [28274, 29533],
"ORF10"           : [29558, 29674],
"three_prime_UTR" : [29675, 29903]
}


# For the sake of speed I build a "huge" dictionary. It's possible because small genome
coord2gene = { i : "genomic" for i in range(1, 29903 + 1) } # Init to genomic
for gene in SCov2_annotation:
    start, end = SCov2_annotation[gene]
    for i in range(start, end + 1):
        coord2gene[i] = gene


def define_windows(args):

    # Define default windows
    windows = [ i for i in range(1, 29903, args.window) ]
    # Extend bases of the last window
    windows[-1] = 29903

    return windows


def parse_args():
    '''
    Parse arguments given to script
    '''    

    parser = argparse.ArgumentParser(description="Calculate mean and median"
    " coverage for COVID19 samples across windows, annotated genes and ARTIC"
    " primers")
    parser.add_argument("-i", "--input-cov-file", dest="covfile", metavar=".depth file",
        required=True)
    parser.add_argument("-p", "--primer-scheme-bed", dest="scheme", metavar="nCoV-2019.scheme.bed",
        required=True)
    parser.add_argument("-w", "--window", dest="window", default=100,
        metavar="100", type=int)

    parser.add_argument("-H", "--print-header", dest="print_header", action="store_true")

    #parser.add_argument("--midpoint", dest="midpoint", action="store_true",
    #help="Whether midpoint of the windows should be output instead of the range (e.g 500 instead of 0:1000)")

    parser.add_argument("-s", "--sample_name", dest="sname",
    help="Sample name for the header. Default: sample", default="sample")

    args = parser.parse_args()

    return args


def parse_samtools_depth(args):
    '''
    Parses the samtools depth file and also warns if that file
    is empty like in the case of negative samples. It creates a wdepth file
    with a special header so its read by the following script COVID19-QC_ivar
    '''

    with open(args.covfile) as infile:
        position_depth = [ int(line.rstrip().split()[2]) for line in infile ]

    if len(position_depth) < 1:
        sys.stdout.write("Empty coverage file\n".format(args.sname))
        sys.exit()

    return position_depth


def parse_primer_scheme(args):

    scheme = {}
    with open(args.scheme) as infile:
        for line in infile:
            chr, start, end, primer, num, strand = line.rstrip().split()
            scheme[primer] = [int(start), int(end)]

    return scheme


def calculate_windows(args):

    position_depth = parse_samtools_depth(args)
    scheme = parse_primer_scheme(args)
    windows = define_windows(args)

    #Calc overall mean and median
    overall_mean = np.mean(position_depth)
    overall_std = np.std(position_depth)
    overall_median = np.median(position_depth)
    Q1 = np.percentile(position_depth, 25)
    Q3 = np.percentile(position_depth, 75)
    IQR = Q3 - Q1
    genomic_windows = {}
    for i in range(0, len(windows) - 1 ):
        start = windows[i]
        end = windows[i+1]
        # Windows are 1:101 and therefore position_depth[start - 1 :end - 1] to
        # take 0:99 equivalent to 1:100
        # but primer coords are exact e.g 1:100 so only
        # take 1 from start (0:100 to take 0:99 positions equivalent to 1:100
        # so position_depth[start - 1 : end ]
        window_mean =   np.mean(position_depth[start - 1: end - 1])
        window_median = np.median(position_depth[start - 1: end - 1])
        # Calculate the mid coordinate of the window e.g 50 for 0:100 window
        midpoint = start + (args.window / 2) - 1 # between 1 and 100 midpoint
        # should be 50 and not 51
        # Look if midpoint is within an annotated gene, otherwise "genomic"
        genomic_windows[midpoint] = (coord2gene[midpoint], window_mean, window_median)

    primer_windows = {}
    for primer in scheme:
        start, end =    scheme[primer]
        # Windows are 1:101 and therefore position_depth[start - 1 :end - 1] to
        # take 0:99 equivalent to 1:100
        # but primer coords are exact e.g 1:100 so only
        # take 1 from start (0:100 to take 0:99 positions equivalent to 1:100
        # so position_depth[start - 1 : end ]
        primer_mean =   np.mean(position_depth[start - 1 : end])
        primer_median = np.median(position_depth[start - 1 :end])
        midpoint = (start + ((end - start) / 2)) - 1 # between 1 and 100 midpoint
        # should be 50 and not 51
        primer_windows[midpoint] = (start, end, primer, primer_mean, primer_median)

    return (genomic_windows, primer_windows, overall_mean, overall_std, overall_median, IQR)


def write_calc_coverage(args):

    genomic_windows, primer_windows, overall_mean, std, overall_median, IQR = calculate_windows(args)

    if args.print_header:
        HEADER = "Sample\twindow_midpoint\twindow\ttype\tannotation\tmean\tmedian\toverall_mean\tstd\toverall_median\tIQR\n"
        sys.stdout.write(HEADER)

    for midpoint in genomic_windows:
        start = midpoint - (args.window / 2)
        end = start + args.window
        window = "{}:{}".format(start, end)
        annotation, mean, median = genomic_windows[midpoint]
        line = "{}\t{}\t{}\tgene\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(args.sname,
                                            midpoint,
                                            window,
                                            annotation,
                                            mean,
                                            median,
                                            overall_mean,
                                            std,
                                            overall_median,
                                            IQR)
        sys.stdout.write(line)

    for midpoint in primer_windows:
        start, end, annotation, mean, median = primer_windows[midpoint]
        window = "{}:{}".format(start, end)
        line = "{}\t{}\t{}\tprimer\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(args.sname,
                                            midpoint,
                                            window,
                                            annotation,
                                            mean,
                                            median,
                                            overall_mean,
                                            std,
                                            overall_median,
                                            IQR)
        sys.stdout.write(line)


def main():

    args = parse_args()
    write_calc_coverage(args)

    return 0


if __name__ == '__main__':
    main()
