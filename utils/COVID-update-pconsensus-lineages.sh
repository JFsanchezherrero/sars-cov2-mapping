#!/usr/bin/sh
date=$(date +%y%m%d)
cd /data2/COVID19/analysis/general_results/Pseudoconsensus
rm ./ALL_pseudoconsensus*
# Concatenate all pseudoconsensus from available runs that pass 90% coverage
cat wuhan_strain.fasta > ALL_pseudoconsensus_${date}.fasta
cd /data2/COVID19/analysis/
ls | grep -v "general_results" | while read user
  do
    cd ${user}/illumina/
    ls | while read RUNID
      do
        # Read QC summaries so only samples with >90% are selected
        tail -n +2 ${RUNID}/reports/QC_summary.${RUNID}.txt |
         grep "^COV" | # to avoid negative controls etc
         awk -v FS=";" '$5 >= 0.9' | 
           cut -f1 -d';' | while read COV
           do
             cat ${RUNID}/pseudoconsensus/${COV}.pseudoconsensus.fasta >> \
             /data2/COVID19/analysis/general_results/Pseudoconsensus/ALL_pseudoconsensus_${date}.fasta
           done
      done
    cd /data2/COVID19/analysis/
  done

cd  /data2/COVID19/analysis/general_results/Pseudoconsensus/
mv pangolin/* pangolin/old/
source activate /home/galo/anaconda3/envs/pangolin
pangolin ALL_pseudoconsensus_${date}.fasta -o pangolin/ \
         --outfile Pseudoconsensus_${date}_lineages.csv \
         -t 12 --max-ambig 0.7
source deactivate

# Generate a csv to update the DB
echo "ID,Lineage" > pangolin/linages_DB_update_${date}.csv
tail -n +2 pangolin/Pseudoconsensus_${date}_lineages.csv |
  grep -v "^hCoV-19" | #Discard the reference genome 
    cut -f1,2 -d',' >> pangolin/linages_DB_update_${date}.csv
