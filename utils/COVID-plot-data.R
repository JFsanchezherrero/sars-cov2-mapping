#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
plotdir <- args[1]
plotdir <- paste(plotdir, "/", sep="")
setwd(plotdir)
input_csv <- args[2]

print(plotdir)
print(input_csv)
library(ggplot2)
library(RColorBrewer)
library(patchwork)
df <- read.table(file = input_csv, sep=";", header=T, stringsAsFactors = T, comment.char='')
df$Fecha <- as.Date(df$Fecha)
df$Status <- as.character(df$Status)
df$Linaje <- as.character(df$Linaje)
  df[which(df$Linaje == "None"), "Linaje"] <- "NA"
df$Linaje <- as.factor(df$Linaje)
df[which(df$Status == "failed_lab" | df$Status == "failed_qc" | df$Status == "sequenced"), "Status"]  <- "Procesadas" 
df[which(df$Status == "in_progress"), "Status"] <- "En proceso"
df[which(df$Status == "received"), "Status"] <- "Recibidas"
df$Status <- as.factor(df$Status)
# Order status as desired
df$Status <- factor(df$Status, levels = c( "Procesadas", "En proceso", "Recibidas"))


#####################################
#           PLOT COLORS
#####################################
### Hist samples
samples_colors <- c("En proceso" = "#eece58", "Procesadas" = "#12ecb4", "Recibidas" =  "#e14468" )

### Sex
sex_colors <- c("Hombre" = "#db4550", "Mujer" = "#54cdbe", "Desconocido" = "#696969")

### LINEAGE
colourCount = length(unique(df[which(!is.na(df$Linaje)), "Linaje"]))
getPalette = colorRampPalette(brewer.pal(9, "Set1"))
linajes <- levels(df[which(!is.na(df$Linaje)), "Linaje"])
colors = getPalette(colourCount)
lincolors <- c()
for(i in c(0:length(linajes))){
  lincolors[as.character(linajes[i])] = colors[i]
}
lincolors["None"] = "#6c6c6c"

### HIOSPITAL
colourCount = length(unique(df[which(!is.na(df$Hospital)), "Hospital"]))
getPalette = colorRampPalette(brewer.pal(9, "Set1"))
hospitals <- levels(df[which(!is.na(df$Hospital)), "Hospital"])
colors = getPalette(colourCount)
hospcolors <- c()
for(i in c(0:length(hospitals))){
  hospcolors[as.character(hospitals[i])] = colors[i]
}

#####################################
#          SAMPLE DATE PLOTS
#####################################
ggplot(df, aes(x=Fecha, fill=Status)) + geom_histogram(binwidth = 1) + 
  theme_bw() + theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  scale_fill_manual(values = samples_colors) + 
  ylab("Número de muestras") + xlab("Fecha toma de muestra") + 
  facet_wrap(~ Hospital, scales = "free") 

  outfile <- paste(plotdir, "general_sample_date.png", sep="")
ggsave(outfile, 
       width = 40,
       height = 20,
       units = "cm",
       dpi = 300)

# Generate the same plot per hospital
for (hospital in levels(df$Hospital)) 
{
  ggplot(df[which(df$Hospital == hospital), ], aes(x=Fecha, fill=Status)) + 
    geom_histogram(binwidth = 1) + 
    theme_bw() + 
    theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
    scale_fill_manual(values = samples_colors) + 
    ylab("Número de muestras") +
    xlab("Fecha toma de muestra")
  
   # outfile <- paste(plotdir, "/sample_date/",hospital, "_sample_date.png", sep="")
  outfile <- paste(plotdir, hospital, "_sample_date.png", sep="")
  ggsave(outfile, dpi = 300)
}

##################################### 
#           CT vs COV
#####################################

cte <- df[which(!is.na(df$CtE) & !is.na(df$Coverage)), ]
ctmax <- max(cte[which(cte$CtE < 90), "CtE"])
cte[which(cte$CtE >= 90), "CtE"] <- ctmax

ctr <- df[which(!is.na(df$CtR)& !is.na(df$Coverage)), ]
ctmax <- max(ctr[which(ctr$CtR < 90), "CtR"])
ctr[which(ctr$CtR >= 90), "CtR"] <- ctmax

ctn <- df[which(!is.na(df$CtN)& !is.na(df$Coverage)), ]
ctmax <- max(ctn[which(ctn$CtN < 90), "CtN"])
ctn[which(ctn$CtN >= 90), "CtN"] <- ctmax

E <- ggplot(cte, aes(x=CtE, y=Coverage)) + geom_point() +
  theme_bw() + scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1)) + 
  scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
  #scale_color_manual(values = hospcolors) +
  xlab("PCR Ct gen E") +
  ylab("Cobertura genómica") +
  ggtitle("Relación entre el PCR CT y la cobertura genómica obtenida") + 
  theme(legend.position = "None") +
  geom_hline(yintercept=0.9, linetype="dashed", 
             color = "black") +
  geom_vline(xintercept = 33, linetype="dashed")

R <- ggplot(ctr, aes(x=CtR, y=Coverage)) + geom_point() +
  theme_bw() + scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1)) + 
  scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
  # scale_color_manual(values = hospcolors) +
  xlab("PCR Ct gen R") + theme(legend.position = "None") +
  ylab(element_blank()) 

N <- ggplot(ctn, aes(x=CtN, y=Coverage)) + geom_point() +
  theme_bw() + scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1)) + 
  scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
  # scale_color_manual(values = hospcolors) +
  xlab("PCR Ct gen N")  +
  ylab(element_blank())

E+R+N
  ggsave(paste(plotdir, "general_Ct_vs_Cov.png", sep=""), 
       width = 60,
       height = 20,
       units = "cm",
       dpi = 300)

for (hospital in levels(df$Hospital)) {
  E <- ggplot(cte[which(cte$Hospital == hospital),], aes(x=CtE, y=Coverage)) + geom_point() +
    theme_bw() + scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1)) + 
    scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
    xlab("PCR Ct gen E") +
    ylab("Cobertura genómica") +
    ggtitle("Relación entre el PCR CT y la cobertura genómica obtenida") + 
    theme(legend.position = "None") +
    geom_hline(yintercept =0.9, linetype="dashed", color = "black") +
    geom_vline(xintercept = 33, linetype="dashed")
  
  R <- ggplot(ctr[which(ctr$Hospital == hospital),], aes(x=CtR, y=Coverage)) + geom_point() +
    theme_bw() + scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1)) + 
    scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
    xlab("PCR Ct gen R") + theme(legend.position = "None") +
    ylab(element_blank()) 
  
  N <- ggplot(ctn[which(ctn$Hospital == hospital),], aes(x=CtN, y=Coverage)) + geom_point() +
    theme_bw() + scale_y_continuous(limits = c(0, 1), breaks = seq(0,1,0.1)) + 
    scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
    xlab("PCR Ct gen N")  +
    ylab(element_blank())
  
  E+R+N
    
    outfile <- paste(plotdir, hospital, "_Ct_vs_Cov.png", sep="")
    ggsave(outfile, 
           width  = 60,
           height = 20,
           units  = "cm",
           dpi    = 300)
}

#####################################
#          LINEAGE PLOTS
#####################################
colourCount = length(unique(df[which(!is.na(df$Linaje)), "Linaje"]))
getPalette = colorRampPalette(brewer.pal(9, "Set1"))
linajes <- levels(df[which(!is.na(df$Linaje)), "Linaje"])
colors = getPalette(colourCount)
lincolors <- c()
for(i in c(0:length(linajes)))
{
  lincolors[as.character(linajes[i])] = colors[i]
}

lincolors["None"] = "#6c6c6c"
ggplot(df[which(!is.na(df$Linaje)),], aes(x="", fill=Linaje)) +
  geom_histogram(stat="count", position = "fill", color = "white") +
  stat_count(geom="text", position = position_fill(vjust = 0.5), aes(label=..count..))+
  theme_bw() + theme(axis.title.y = element_blank(),
                     axis.title.x = element_blank(),
                     axis.line = element_blank(),
                     axis.text = element_blank(),
                     axis.ticks = element_blank()) +
  scale_fill_manual(values = lincolors) + facet_wrap(~Hospital, ncol = 4) +
  coord_polar("y", start=0)


ggsave(paste(plotdir, "general_lineages.png", sep=""), 
       width = 40,
       height = 30,
       units = "cm",
       dpi = 300)


for (hospital in levels(df$Hospital)) 
{
  lincount <- length(unique(df[which(!is.na(df$Linaje) & df$Hospital == hospital), "Linaje"]))
  if(lincount > 0)
  {
    ggplot(df[which(!is.na(df$Linaje)& df$Hospital == hospital),], aes(x="", fill=Linaje)) +
    geom_histogram(stat="count", position = "fill", color = "white") +
    stat_count(geom="text", position = position_fill(vjust = 0.5), aes(label=..count..))+
    theme_bw() + theme(axis.title.y = element_blank(),
                       axis.title.x = element_blank(),
                       axis.line = element_blank(),
                       axis.text = element_blank(),
                       axis.ticks = element_blank()) +
    scale_fill_manual(values = lincolors) +
    coord_polar("y", start=0)
  
    outfile <- paste(plotdir, hospital, "_lineages.png", sep="")
    ggsave(outfile, dpi = 300)
  }
}


######################################
#       INFORME CT CONSORCIO
######################################
ggplot(cte, aes(x=CtE, y=Coverage)) + geom_point() +
  theme_bw() + 
  scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1), 
                     labels = c("0%", "10%","20%","30%","40%","50%",
                                "60%","70%","80%","90%","100%")) + 
  scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
  xlab("PCR Ct gen E") +
  ylab("Cobertura genómica") +
  ggtitle("Relación entre el PCR CT y la cobertura genómica obtenida") + 
  theme(legend.position = "None") +
  geom_hline(yintercept =0.9, linetype="dashed", color = "black") +
  geom_vline(xintercept = 33, linetype="dashed")

ggplot(ctn, aes(x=CtN, y=Coverage)) + geom_point() +
  theme_bw() + 
  scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1), 
                     labels = c("0%", "10%","20%","30%","40%","50%",
                                "60%","70%","80%","90%","100%")) + 
  scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
  xlab("PCR Ct gen E") +
  ylab("Cobertura genómica") +
  ggtitle("Relación entre el PCR CT y la cobertura genómica obtenida") + 
  theme(legend.position = "None") +
  geom_hline(yintercept =0.9, linetype="dashed", color = "black") +
  geom_vline(xintercept = 33, linetype="dashed")

ggplot(ctr, aes(x=CtR, y=Coverage)) + geom_point() +
  theme_bw() + 
  scale_y_continuous(limits = c(0, 1),breaks = seq(0,1,0.1), 
                     labels = c("0%", "10%","20%","30%","40%","50%",
                                "60%","70%","80%","90%","100%")) + 
  scale_x_continuous(limits = c(10, 45), breaks = seq(10,45,2)) +
  xlab("PCR Ct gen E") +
  ylab("Cobertura genómica") +
  ggtitle("Relación entre el PCR CT y la cobertura genómica obtenida") + 
  theme(legend.position = "None") +
  geom_hline(yintercept=0.9, linetype="dashed", color = "black") +
  geom_vline(xintercept = 33, linetype="dashed")
