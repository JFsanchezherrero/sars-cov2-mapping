#!/bin/bash
###############################################################################
## Usage (1): COVID-update-QC_summaries_LONG.sh <SEQPLACE> <RUNID>
## Usage (2): COVID-update-QC_summaries_LONG.sh -all
##
## Arguments:
##   <SEQPLACE> Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>    Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
## 
## Description:
##   (1). Reanalize the QC_Summary for the given SEQPLACE and RUNID
##   (2). Reanalize all the QC_Summary files for all SEQPLACEs and RUNIDs
##
##   As results, generates the next set of files:
##     [...]/analysis/[...]/reports/QC_summary.${RUNID}.long.csv => With more stats than the QC
##   
##   If -all flag is enabled, it also generates the next files:
##     ALL_QC_SUMMARY.short.csv => Contains all the results for all the runs (Short output)
##     ALL_QC_SUMMARY.long.csv  => Contains all the results for all the runs (Long  output)
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 25/09/2020
## __version__   = 2.0.0
##
###############################################################################



function showUsage()
{
	echo -e 'Usage (1): COVID-update-QC_summaries_LONG <SEQPLACE> <RUNID>'
    echo -e 'Description: Reanalize the QC_Summary for the given SEQPLACE and RUNID'
    echo -e 'Sample: COVID-reanalysis-QC_Summary.sh FISABIO 200824_M02956'
    echo -e ''
    echo -e 'Usage (2): COVID-update-QC_summaries_LONG -all'
    echo -e 'Description: Reanalize all the QC_Summary files for all SEQPLACEs and RUNIDs'
    echo -e ''	
	exit -1
}

function write_header_short()
{        
    header=$(head -n 1 $QCIN)    
    # Output the header
    echo -e $header > $1
}

function write_header_long()
{   
    # QCIN variable must be set     
    header=$(head -n 1 $QCIN)

    # Output the results
    echo -e -n $header > $1
    echo -e -n ';Median_Pool_01;Median_Pool_02' >> $1

    for i in {1..9}; do
        echo -e -n ';Median_Amp_0'$i >> $1
    done

    for i in {10..98}; do
        echo -e -n ';Median_Amp_'$i >> $1
    done

    # Write the end line
    echo '' >> $1
}

function doAnalysis()
{
    # Debug
    echo -e '\tQC_Summary reanalysis long dataset => \t SeqPlace: '$SEQPLACE'\t RunId: '$RUNID

    # Check for FISABIO seq place
    if [ "${SEQPLACE}" = "FISABIO" ]; then
    	SEQPLACE='seqfisabio'
    fi

    # Get the analysis folder
    workfolder=${BASEPATH}'/'${SEQPLACE}'/illumina/'${RUNID}

    # Check that the working folder exist
    if [ ! -d ${workfolder} ]; then
        echo -e '[ERROR] Analysis Folder does not exist -> ' ${workfolder}
        return
    fi

    # Get the input QC summary
    QCIN=${workfolder}'/reports/QC_summary.'${RUNID}'.csv'

    # Create the output file paths
    QCOUT01=${workfolder}'/reports/QC_summary.'${RUNID}'.long.csv'

    # Check that the QC Summary file exist
    if [ ! -f ${QCIN} ]; then
        echo -e '[ERROR] Input File does not exist -> ' ${QCIN}
        showUsage
    fi

    # Write the file headers
    write_header_long $QCOUT01

    # Parse each line of the QC_Summary
    while read line
    do
        cov=$(echo $line | cut -f1 -d';')

        # Jump header
        if [ "$cov" = "sample" ]; then
            continue
        fi

        # Build the input full paths
        path_adepth=${workfolder}'/reports/'${cov}'.adepth'

        # Check adepth is a file
        if [ ! -f $path_adepth ]; then
            echo '[WARNING]: ['$cov'] Adepth not found => '$path_report
            continue
        fi

        # Get the info from the adepth file
        median_amplicons=$(cat  $path_adepth | grep median |  cut -f8 -d';') #| wc)
        median_pool01=$(cat  $path_adepth | grep median | grep Pool_1 | cut -f9 -d';' | uniq)
        median_pool02=$(cat  $path_adepth | grep median | grep Pool_2 | cut -f9 -d';' | uniq)
        num_stats98=$(echo $median_amplicons | wc -w)
        num_statsp1=$(echo $median_pool01 | wc -w)
        num_statsp2=$(echo $median_pool02 | wc -w)

        # Check for the correct number of amplicons
        if [ $num_stats98 -ne 98 ]; then
            echo '[WARNING]: ['$cov'] Wrong number of median stats for amplicons => '$num_stats98
            median_amplicons='ERROR'
        fi
        if [ $num_statsp1 -ne 1 ]; then
            echo '[WARNING]: ['$cov'] Wrong number of stats for Pool01 => '$num_statsp1
            median_pool01='ERROR'
        fi
        if [ $num_statsp2 -ne 1 ]; then
            echo '[WARNING]: ['$cov'] Wrong number of stats for Pool02 => '$num_statsp2
            median_pool02='ERROR'
        fi

        # Check non empty variables
        if [ -z "$median_pool01" ]; then
            median_pool01='ERROR'
        fi
        if [ -z "$median_pool02" ]; then
            median_pool02='ERROR'
        fi
        if [ -z "$median_amplicons" ]; then
            median_amplicons='ERROR'
        fi

        # Debug?
        if [ $VERBOSE -eq 1 ]; then
            echo -e '\t\t COV:' $cov '\t#Amplicons =' $num_stats98'\t '
        fi
        
        # 1 - Output the results to the Long output file
        echo -e -n $line >> $QCOUT01

        # Include the median for each pool (1 & 2)
        echo -e -n ';'$median_pool01';'$median_pool02 >> $QCOUT01

        # Include the median for each amplicon (98 items)
        for i in $median_amplicons; do
            echo -e -n ';'$i >> $QCOUT01
        done

        # Write the final end of line
        echo -e '' >> $QCOUT01

    done < ${QCIN}

    # Debug
    if [ $VERBOSE -eq 1 ]; then
        echo -e '\t[DONE] Results saved to -> '
        echo -e '\t ' ${QCOUT01}
        echo -e ''
    fi
}



# Global variables
SEQPLACE=null  # Sequencing place
RUNID=null     # Run Id
FLAG_ALL=1     # 1=YES, 0=NO
VERBOSE=1      # 1=YES, 0=NO
BASEPATH=/data2/COVID19/analysis

if [ $# -eq 2 ];then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ "$1" = "-all" ]; then
    FLAG_ALL=1
    VERBOSE=0    # In this mode the verbose mode is off
else
	echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
	showUsage
fi


# Perform only one reanalysis for the given seqplace & run id
if [ $FLAG_ALL -eq 0 ]; then
    doAnalysis
    exit 0
fi

# Check for errors
if [ $FLAG_ALL -ne 1 ]; then
    echo '[ERROR]: Unknown command options'
    showUsage
fi

# From here, it is assumed the -all flag
OUTFILE01='ALL_QC_SUMMARY.short.csv'
OUTFILE02='ALL_QC_SUMMARY.long.csv'

# Write the file headers
HEADER_WRITTEN=0

# For each sequencing place folder
for seq_place in $(ls $BASEPATH)
do

    # Get the full path to the Sequencing Place
    path01=${BASEPATH}'/'$seq_place

    # Ensure directory
    if [ ! -d $path01 ]; then
        continue
    fi

    # Jump general_results
    if [ "$seq_place" = "general_results" ]; then
        continue
    fi

    # Get a new variable with the Real Sequencing Place
    real_seq_place=$seq_place

    # FISABIO seq place exception
    if [ "$seq_place" = "seqfisabio" ]; then
        real_seq_place="FISABIO"
    fi    

    # For each run folder
    for runid in $(ls $path01/illumina)
    do
        path02=$path01'/illumina/'$runid

        # Ensure directory
        if [ ! -d $path02 ]; then
            continue
        fi

        # Set the global variables and perform the analysis
        SEQPLACE=$real_seq_place  # Sequencing place
        RUNID=$runid
        doAnalysis

        # Write the file headers for the total file?
        if [ $HEADER_WRITTEN -eq 0 ]; then
            write_header_short $OUTFILE01
            write_header_long  $OUTFILE02
            HEADER_WRITTEN=1
        fi

        # Write to the total output files jumping the header        
        tail -n +2 ${QCIN}    >> ${OUTFILE01}
        tail -n +2 ${QCOUT01} >> ${OUTFILE02}
        
    done # end runid

done # end seqplace


# Debug
echo -e '[FINAL DONE] Total results saved to -> '
echo -e '\t ' ${OUTFILE01}
echo -e '\t ' ${OUTFILE02}
echo -e ''