#!/usr/bin/bash
###############################################################################
## Usage: COVID-microreact-submission.sh
##
## 
## Description:
##   Prepares a new microreact submission with the current DB data
##   As results, generates a new folder containing:
##     .csv    file: Sample metadata
##     .fasta  file: Concatenated aligned and masked consensus
##     .list   file: Full paths used to create the fasta file
##     .tree   file: File to upload to MICROREACT
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 12/11/2020
## __version__   = 1.0.0
##
###############################################################################

# Auxiliar functions ##########################################################
function showUsage()
{
    echo -e 'Usage: ' $1 
    echo -e '\t Prepares a new microreact submission with the current DB data'
    exit -1
}
###############################################################################


# Preconditions ###############################################################
# Check for input arguments
if [ $# != 0 ]; then
    showUsage $0
fi
###############################################################################

# Initialization ##############################################################
# Constants
date=$(date +%Y%m%d)
BASE_PATH='/data2/COVID19/analysis/general_results/MICROREACT'
SUBMISSION_FOLDER='MICROREACT_submission_'${date}
PYSCRIPT='/data2/COVID19/software/sars-cov2-mapping/utils/COVID_microreact_submission.py'
ref_file='/data2/COVID19/reference_data/root_Wuhan.masked.fasta'

# Output file paths
CSV_PATH=${BASE_PATH}'/'${SUBMISSION_FOLDER}'/MICROREACT_'${date}'_metadata.csv'
FLI_PATH=${BASE_PATH}'/'${SUBMISSION_FOLDER}'/MICROREACT_'${date}'_fasta.list'
FAS_PATH=${BASE_PATH}'/'${SUBMISSION_FOLDER}'/MICROREACT_'${date}'.aln.masked.fasta'


# Check that the reference exist
if [ ! -f $ref_file ]; then
    echo 'ERROR: Reference file does not exist:' $ref_file
    exit -1
fi

# Move to the base folder and create the directory
cd ${BASE_PATH}
if [ ! -d ${SUBMISSION_FOLDER} ]; then
    mkdir ${SUBMISSION_FOLDER}
fi
cd ${SUBMISSION_FOLDER}
###############################################################################


###############################################################################
# (1/2) - Create the csv and the fli files with the python script
${PYSCRIPT} -silent 1 -csv ${CSV_PATH} -fli ${FLI_PATH}
###############################################################################


###############################################################################
# (2/3) - Create the Consensus fasta (alignment)
###############################################################################

# Create the out_file with the root reference
cat $ref_file > ${FAS_PATH}

echo -e '\t => Appending fasta consensus to' ${FAS_PATH}
for cov_fasta in $(cat $FLI_PATH)
do
    # Check that the file exists
    if [ ! -f ${cov_fasta} ]; then
        echo -e "[ERROR]: consensus not found => " $i
        continue
    fi

    # Concatenate this files in the single multifasta
    cat ${cov_fasta} >> ${FAS_PATH}
done
echo -e '\t => Appending fasta consensus [DONE]'

# Success log
echo -e '[SUCCESS]'
echo -e '\t Microreact CSV file: ' ${CSV_PATH}
echo -e '\t Consensus file list: ' ${FLI_PATH}
echo -e '\t Consensus fasta:     ' ${FAS_PATH}
echo -e 
###############################################################################


###############################################################################
# (3/3) - Call the tree [CZB: YES, BOOTSTRAP: NO]
###############################################################################

# Create a new tree folder to work there
TREE_FOLDER='tree'
if [ ! -d ${TREE_FOLDER} ]; then
    mkdir ${TREE_FOLDER}
fi
cd ${TREE_FOLDER}


# Set the tree variables
alineamiento=${FAS_PATH}
treeprefix='MICROREACT_'${date}'_TREE'
ncpus=25
nicevalue=3
iqtreecmd='/data/Software/iqtree-1.6.10-Linux/bin/iqtree'
prefix=${treeprefix}'.czb_yes_boot_no'

# Debug starts
echo '[START TREES => CZB (YES) BOOTSTRAP (NO)]:' ${alineamiento} ${prefix}

# Run the tree process
nice -n ${nicevalue}        \
     ${iqtreecmd}           \
         -s ${alineamiento} \
         -nt ${ncpus}       \
         -m GTR             \
         -czb               \
         -pre ${prefix}     \
         -o hCoV-19_Wuhan_IPBCAMS-WH-01_2019_EPI_ISL_402123_2019-12-24

# Rename output to nwk extension in order to allow submit to microreact
cp ${prefix}'.treefile' ${prefix}'.nwk'

# Debug end
echo -e '[DONE TREE => CZB (YES) BOOTSTRAP (NO)]: ' 
echo -e ' \t Alignment: ' ${alineamiento} 
echo -e ' \t Prefix: '    ${prefix}
###############################################################################
