#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Write updated reports for SeqCOVID-SPAIN project
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import os
import codecs
import sys
import argparse
import COVID_Utils             as cutils
import COVID_loadDBCredentials as dbcredentials


def parse_args():
    '''
    Parse arguments given to script
    '''
    
    parser = argparse.ArgumentParser(
        description="Write updated reports for SeqCOVID-SPAIN project")
    parser.add_argument("-o", "--output-directory", dest="odir", required=True)
    args = parser.parse_args()
    return args


def get_consensus_file(cov, seq_place, run_code, seq_tech):
    '''
    Get the consensus file path from the given parameters
    '''

    analysis_folder = cutils.getAnalysisFolder(seq_place, run_code, seq_tech)    
    return analysis_folder + "/consensus/" + cov + ".fa"


def get_hospital_dirname(hospital):
    '''
    Gets the Hospital folder name for the given hospital
    '''
    tokens = hospital.split("(")
    hospformat = tokens[0].strip("_").replace("/", "_")
    tokens = tokens[1].split(")")
    hosp_code = tokens[0]
    return hosp_code + '_' + hospformat


def read_DB():
    
    # Gets a new connection to the DB
    cnx = dbcredentials.new_connection()

    # Create a cursor
    mycursor = cnx.cursor()

    # Run the SQL sentence
    mycursor.execute(
        "SELECT "            + \
        "  s.gId, "          + \
        "  s.gender, "       + \
        "  s.status, "       + \
        "  s.lineage, "      + \
        "  s.coverage, "     + \
        "  s.median_depth, " + \
        "  s.pcr_e_c, "      + \
        "  s.gisaid, "       + \
        "  s.gisaid_epi, "   + \
        "  h.hosp_code, "    + \
        "  h.name, "         + \
        "  s.shipment_shipping_code, "  + \
        "  s.hospital_date, "           + \
        "  s.pcr_r_c, "                 + \
        "  s.pcr_n_c, "                 + \
        "  s.hospital_sample_number, "  + \
        "  s.seq_place, "               + \
        "  s.run_code, "                + \
        "  s.seq_tech "                 + \
        " FROM sample s "               + \
        " INNER JOIN hospital h "       + \
        " ON s.hospital_id = h.id "     + \
        " WHERE "                       + \
        "  h.deleted_dt LIKE \"\" AND " + \
        "  s.deleted_dt LIKE \"\" ")

    SQL_info = mycursor.fetchall()
    mycursor.close()

    # Look for each hospital how many shipments have been received and how many are pending
    mycursor = cnx.cursor() #creo un cursor
    mycursor.execute(
        "SELECT h.name, sh.shipping_code, sh.received " + \
        "FROM shipment sh INNER JOIN hospital h "       + \
        "ON h.id = sh.hospital_id "                     + \
        "WHERE "                                        + \
        " h.deleted_dt  LIKE \"\" AND"                  + \
        " sh.deleted_dt LIKE \"\" ")

    SQL_shipment = mycursor.fetchall()
    mycursor.close()
    cnx.close()

    DB        = {}
    SHIPMENTS = {}
    for record in SQL_info:
        
        # Build COV identifier from numeric identifier
        cov    = cutils.number2cov(record[0])
        sex    = cutils.getSafeGender(record[1])
        status = record[2].lower().replace(" ", "_")
        
        lineage = cutils.getSafeValue(record[3])
        lineage = lineage.strip(".") # a bug in pangolin puts extra dots after lineage        

        coverage    = cutils.getSafeRoundedFloat(record[4])        
        depth       = cutils.getSafeValue(record[5])
        pcr_ct      = cutils.getSafeValue(record[6])
        gisaid      = cutils.getSafeValue(record[7])
        gisaid_epi  = cutils.getSafeValue(record[8])
        hosp_code   = cutils.getSafeValue(record[9])
        hospital    = record[10].replace(" ", "_")
        shipcode    = cutils.get_shipcode(record[11])
        date        = cutils.getSafeDate( record[12])
        pcr_r_c     = cutils.getSafeValue(record[13])
        pcr_n_c     = cutils.getSafeValue(record[14])
        hosp_number = cutils.getSafeValue(record[15])
        seq_place   = cutils.getSafeValue(record[16]) 
        run_code    = cutils.getSafeValue(record[17])
        seq_tech    = cutils.getSafeValue(record[18])

        # Do not set lineage for samples with coverage less than 75%        
        if not type(coverage) is str and coverage < 0.75:
            lineage = 'None'

        # Check if the hospital exists in the DB dictionary
        if hospital not in DB:
            DB[hospital] = {}
            #DB[hospital]['hosp_code'] = hosp_code

        # Append the new record to the DB
        DB[hospital].update( { cov : {
                                "sex"         : sex,
                                "status"      : status,
                                "lineage"     : lineage,
                                "coverage"    : coverage,
                                "depth"       : depth,
                                "pcr_ct"      : pcr_ct,
                                "pcr_r_c"     : pcr_r_c,
                                "pcr_n_c"     : pcr_n_c,
                                "gisaid"      : gisaid,
                                "gisaid_epi"  : gisaid_epi,
                                "shipcode"    : shipcode,
                                "date"        : date,
                                "hosp_number" : hosp_number,
                                "seq_place"   : seq_place,
                                "run_code"    : run_code,
                                "seq_tech"    : seq_tech }
                                })

        if hospital not in SHIPMENTS:
            SHIPMENTS[hospital] = {}

        if shipcode not in SHIPMENTS[hospital]:
            SHIPMENTS[hospital][shipcode] = {
            "num_samples" : 0,
            "processed"   : 0,
            "in_progress" : 0,
            "sequenced"   : 0,
            "pass_qc"     : 0,
            "failed_qc"   : 0,
            "failed_lab"  : 0,
            "received"    : True # whether the shipment has been received
            }
            
        SHIPMENTS[hospital][shipcode]["num_samples"] += 1
        if status:
            if status == "in_progress":
                SHIPMENTS[hospital][shipcode]["in_progress"] += 1
            elif status != "received":
                SHIPMENTS[hospital][shipcode]["processed"] += 1
            
        else:
            print('Status - ', status)

        if "sequenced" in status.lower():
            SHIPMENTS[hospital][shipcode]["sequenced"] += 1
        elif status == "failed_qc":
            SHIPMENTS[hospital][shipcode]["failed_qc"] += 1
        elif status == "failed_lab":
            SHIPMENTS[hospital][shipcode]["failed_lab"] += 1


    for record in SQL_shipment:
        hospital = record[0].replace(" ", "_")
        shipcode = cutils.get_shipcode(record[1])
        received = record[2]
        if hospital not in SHIPMENTS:
            SHIPMENTS[hospital] = {}
        if shipcode not in SHIPMENTS[hospital]:
            if not received:
                SHIPMENTS[hospital][shipcode] = {
                "num_samples" : 0,
                "in_progress" : 0,
                "processed"   : 0,
                "sequenced"   : 0,
                "pass_qc"     : 0,
                "failed_qc"   : 0,
                "failed_lab"  : 0,
                "received"    : False # whether the shipment has been received
                }
            else:
                sys.stdout.write("Shipment {} ".format(shipcode) \
                                + "is marked as received but no sample was found in the DB\n")

    return DB, SHIPMENTS


def summarize_DB(DB):
    '''
     Reads info from DB dict and summarizes some data
    '''

    summary =  { "grand_total" : {
        "total"        : 0,
        "in_progress"  : 0,
        "fail_qc"      : 0,
        "fail_lab"     : 0,
        # "received"   : 0,
        "sequenced"    : 0,
        "success_seq"  : 0,
        "success_rate" : 0,
        "processed"    : 0
        }
    }

    for hospital in DB:
        num_samples = len(DB[hospital])
        in_progress = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "in_progress"])
        success_seq = len([sample for sample in DB[hospital] if "sequenced" in DB[hospital][sample]["status"] ])
        fail_qc     = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "failed_qc"])
        fail_lab    = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "failed_lab"])
        failed      = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "failed"])
        removed_g   = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "removed_gisaid"])
        nanopore    = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "nanopore"])
        # received  = len([sample for sample in DB[hospital] if DB[hospital][sample]["status"] == "received"])
        # received = received + fail_lab + fail_qc + success_seq + in_progress

        sequenced = success_seq + fail_qc + removed_g + nanopore
        processed = sequenced + fail_lab + failed
        summary[hospital] = {
            "total"        : num_samples,
            "in_progress"  : in_progress,
            "fail_qc"      : fail_qc,
            "fail_lab"     : fail_lab,
            # "received"   : received,
            "sequenced"    : sequenced,
            "success_seq"  : success_seq,
            "success_rate" : "NA",
            "processed"    : processed
        }

        if sequenced > 0:
            success_rate = round(100 * success_seq / float(processed), 2)
            summary[hospital]["success_rate"] = success_rate

        summary["grand_total"]["total"]       += num_samples
        summary["grand_total"]["in_progress"] += in_progress
        summary["grand_total"]["fail_qc"]     += fail_qc
        summary["grand_total"]["fail_lab"]    += fail_lab
        #summary["grand_total"]["received"]   += received
        summary["grand_total"]["sequenced"]   += sequenced
        summary["grand_total"]["success_seq"] += success_seq
        summary["grand_total"]["processed"]   += processed


    # Get the success rate
    nsuccess = float(summary["grand_total"]["success_seq"])
    ntotal   = float(summary["grand_total"]["sequenced"])
    success_rate = 0.0

    if ntotal > 0:
        success_rate = round(100 * nsuccess / ntotal, 2)
    summary["grand_total"]["success_rate"] =  success_rate
    
    return summary


def plot_data(DB, plot_datafile, plotdir):
    '''
    Generate dataframes to be read by R and make plots
    '''
    HEADER = "ID;Sexo;Fecha;Linaje;Coverage;CtE;CtR;CtN;Shipping_Code;Run_Code;Hospital;Status;Hospital_sample_number;GISAID_Virus_Name;GISAID_EPI_Code\n"
    with codecs.open(plot_datafile, mode='w', encoding='utf-8') as outfile:
    #with open(plot_datafile, "w") as outfile:
        outfile.write(HEADER)
        for hospital in DB:

            hospformat = get_hospital_dirname(hospital)

            hospital_csv = open("{}/{}.datos_muestras.csv".format(plotdir, hospformat), "w")
            hospital_csv.write(HEADER)
            for cov in DB[hospital]:
                line = "{};{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(
                cov,
                DB[hospital][cov]["sex"],
                DB[hospital][cov]["date"],
                DB[hospital][cov]["lineage"],
                DB[hospital][cov]["coverage"],
                DB[hospital][cov]["pcr_ct"],
                DB[hospital][cov]["pcr_r_c"],
                DB[hospital][cov]["pcr_n_c"],
                DB[hospital][cov]["shipcode"],
                DB[hospital][cov]["run_code"],
                hospformat,
                DB[hospital][cov]["status"],
                DB[hospital][cov]["hosp_number"],
                DB[hospital][cov]["gisaid"],
                DB[hospital][cov]["gisaid_epi"]
                )
                outfile.write(line)
                hospital_csv.write(line)
            hospital_csv.close()

    return 0


def generate_plots(DB, args):
    
    
    from datetime import date

    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir       = os.path.abspath(args.odir)
    plotdir    = odir    + "/plot_data"
    ctcovdir   = plotdir + "/Ct_vs-Cov/"
    lineagedir = plotdir + "/lineages/"
    sdatedir   = plotdir + "/sample_date/"

    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)
    if not os.path.exists(plotdir):
        os.mkdir(plotdir)
    if not os.path.exists(ctcovdir):
        os.mkdir(ctcovdir)
    if not os.path.exists(lineagedir):
        os.mkdir(lineagedir)
    if not os.path.exists(sdatedir):
        os.mkdir(sdatedir)
    
    # Get the current date
    today = date.today()
    compact_date = "{}{:02d}{:02d}".format(str(today.year)[-2:],
                                               today.month, today.day)

    # Path to the plot data
    plot_datafile = "{}/plot_data_{}.asv".format(plotdir, compact_date)

    # Generate the CSV to plot data
    plot_data(DB, plot_datafile, plotdir)

    # call the Rscript to generate the plots
    pythonscript_path = os.path.dirname(os.path.realpath(__file__))
    rscript_path = "{}/COVID-plot-data.R".format(pythonscript_path)
    os.system("Rscript --vanilla --verbose {} {} {}".format(
        rscript_path,
        plotdir,
        plot_datafile
        ))

    pngs_to_base64(plotdir) # convert png to text to embed within html

    os.system("mv {}/*sample_date.png {}/sample_date/".format(plotdir, plotdir))
    os.system("mv {}/*Ct_vs_Cov.png {}/Ct_vs-Cov/".format(plotdir, plotdir))
    os.system("mv {}/*lineages.png {}/lineages/".format(plotdir, plotdir))
    os.system("rm {}/Rplots.pdf".format(plotdir))

    return 0


def pngs_to_base64(png_dir):
    '''Given a directory with png plots, convert them to txt base64 format'''
    from glob import glob    
    pngs = glob("{}/*.png".format(png_dir))
    basenames = [ png.split(".")[0] for png in pngs ]
    for basename in basenames:
        os.system("base64 {}.png > {}.txt".format(basename, basename))

    return 0


def get_html_img_text(plot_file):
    '''
    Reads the specified file and encapsulates it in a html string image data
    '''

    # Open the html expression
    html_plot_str = '<img src="data:image/png;base64,'

    # Append the file content
    with open(plot_file) as infile:        
        for line in infile:
            html_plot_str += line.strip()

    # Close the html expression
        html_plot_str += '">'

    # Return the string
    return html_plot_str


def get_txt_images(plotdir, hospital):
    '''
    Retrieves png plots in txt format for the specified hospital
    and return them in a dictionary so they can be embedded in the html report
    if hospital is not specified, plots of the General report are retrieved
    '''  
    
    plot_file = "{}/{}_lineages.txt".format(plotdir, hospital)
    lineage_plot = get_html_img_text(plot_file)

    plot_file = "{}/{}_Ct_vs_Cov.txt".format(plotdir, hospital)
    qpcr_plot = get_html_img_text(plot_file)

    plot_file = "{}/{}_sample_date.txt".format(plotdir, hospital)
    samples_plot = get_html_img_text(plot_file)

    images = {
        "<p>IMAGE_LINAJES</p>" : lineage_plot,
        "<p>IMAGE_QPCR</p>"    : qpcr_plot,
        "<p>IMAGE_SAMPLES</p>" : samples_plot
    }

    return images


def per_hospital_report(DB, SHIPMENTS, summary, args):
    '''
    Summary report each hospital
    '''
    
    from datetime import date

    # Get the current date
    today        = date.today()
    compact_date = "{}{:02d}{:02d}".format(str(today.year)[-2:],
                                               today.month, today.day)
    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir = os.path.abspath(args.odir)

    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)

    for hospital in DB:
        
        # Hopital formatted name
        hospformat = get_hospital_dirname(hospital)
        
        # Number of shipments and samples/shipment
        n_shipments = len([ship for ship in SHIPMENTS[hospital]])
        received    = len([ship for ship in SHIPMENTS[hospital] if SHIPMENTS[hospital][ship]["received"]])

        # Info file path
        info_file = "{}/Informe_{}.md".format(odir, hospformat)

        # Open output file
        with codecs.open(info_file, mode='w', encoding='utf-8') as outfile:

            # Report header
            informe = "# Informe de seguimiento SeqCOVID-SPAIN\n" \
            + "\n" \
            + "**Hospital:** {}\n".format(hospformat.replace("_", " ")) \
            + "\n**Fecha de actualización:** {:02d}/{:02d}/{}\n".format(today.day,
                                                                 today.month,
                                                                 today.year) \
            + "\nEn el marco del proyecto SeqCOVID-SPAIN, hemos recibido de su" \
            + " hospital un total de {} muestras.".format(summary[hospital]["total"])\
            + " De estas, {} han sido procesadas ".format(summary[hospital]["processed"])\
            + "y {} ({}%) han sido secuenciadas con éxito".format(
                                                  summary[hospital]["success_seq"],
                                                  summary[hospital]["success_rate"])\
            + " según el criterio de calidad mínimo establecido"\
            + " (se ha leído al menos el 90% del genoma del virus).\n\n" \
            + "A continuación se resume la información sobre los envíos relativos a su hospital:\n" \
            + "| Envío |  Estado | Muestras | En proceso | Procesadas | Seq | Falla QC | Fallo Lab |\n" \
            + "| -------- | ---------- | --------- | ------- | ------- | ------- | ------- | ------- |\n"

            # Shipment table
            for shipcode in SHIPMENTS[hospital]:
                nsamples    = SHIPMENTS[hospital][shipcode]["num_samples"]
                in_progress = SHIPMENTS[hospital][shipcode]["in_progress"]
                processed   = SHIPMENTS[hospital][shipcode]["processed"]
                sequenced   = SHIPMENTS[hospital][shipcode]["sequenced"]
                failed_qc   = SHIPMENTS[hospital][shipcode]["failed_qc"]
                failed_lab  = SHIPMENTS[hospital][shipcode]["failed_lab"]
                if not SHIPMENTS[hospital][shipcode]["received"]:
                    received = "Pendiente"
                else:
                    received = "Recibido"
                informe += "| {} | {} | {} | {} | {} | {} | {} | {} |\n".format(
                shipcode,
                received,
                nsamples,
                in_progress,
                processed,
                sequenced,
                failed_qc,
                failed_lab
                )

            # Report body
            informe += "\n" \
                    + "En el documento excel adjunto " \
                    + "({}.datos_muestras.csv)".format(hospformat) \
                    + " encontrará información detallada sobre cada muestra.\n" \
                    + "\nAdjunto a este informe también ha recibido un archivo" \
                    + " multifasta con los genomas consenso de los virus secuenciados" \
                    + " ({}.consensus.fasta).\n".format(hospformat) \
                    + "\nAsimismo le recordamos que todos los datos están a su" \
                    + " disposición, incluyendo los ficheros brutos de secuenciación," \
                    + " que puede solicitar enviando un correo a bioinfo.covid19@ibv.csic.es\n"

            outfile.write(informe)


            informe = "\n\n" + \
                      "#### Muestras procesadas en función de la fecha\n" + \
                      "En la siguiente figura se muestra el estado de las muestras " + \
                      '("Recibidas", "En Proceso" o "Procesadas") en función de' + \
                      " la fecha que fue proporcionada por su hospital.\n" + \
                      "\nIMAGE_SAMPLES" # IMAGE_SAMPLES string will be replaced within the
                                      # html to embed the actual plot

            informe += "\n\n" + \
                      "#### Relación entre Ct (qPCR) y cobertura genómica\n" + \
                      "En la siguiente figura se muestran los valores de Ct de" + \
                      " la qPCR (en caso de haber sido proporcionados)" + \
                      " y su relación con la cobertura genómica obtenida tras el análisis.\n" + \
                      "\nIMAGE_QPCR"

            informe += "\n\n" + \
                      "#### Linajes de las muestras\n" + \
                      "En la siguiente figura se muestra un resumen del tipado" + \
                      " de las muestras según los linajes definidos por Rambaut _et al._\n" + \
                      "\nIMAGE_LINAJES"

            # Writhe the report footer
            outfile.write(informe)

        # CONVERT REPORT TO HTML
        prefix = info_file.split(".")[0]

        os.system("/usr/bin/grip --user=seqcovid --pass=106fec64516f473507fddafd0d8687938cedf384 --export {}.md {}.html".format(prefix, prefix))
        os.system("rm {}.md".format(prefix))
        #Embed txt imges in html
        embed_html_images(args, prefix, hospformat)

    return 0


def embed_html_images(args, prefix, hospital="general"):
    
    odir = os.path.abspath(args.odir)
    plotdir = odir + "/plot_data"

    try:
        txtimages = get_txt_images(plotdir, hospital)
    except FileNotFoundError:
        # In this case there are no plot files for the hospital because
        # no sample from that hospital has been procesed yet
        return 0

    with open("{}.html".format(prefix)) as infile:
        with open("{}.html_temp".format(prefix), "w") as outfile:
            for line in infile:
                for image in txtimages:
                    if image in line:
                        line = line.replace(image, txtimages[image])
                outfile.write(line)
    os.system("mv {}.html_temp {}.html".format(prefix, prefix))

    return 0


def grand_total_report(summary, SHIPMENTS, args):    
    from datetime import datetime
    from glob import glob

    now = datetime.now()
    compact_date = "{}{:02d}{:02d}".format(str(now.year)[-2:],
                                               now.month, now.day)
    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir = os.path.abspath(args.odir)
    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)
    if not os.path.exists("{}/old_reports".format(odir)):
        os.mkdir("{}/old_reports".format(odir))
    old_reports = glob("{}/Informe_General*.html".format(odir))
    old_shipments = glob("{}/Shipments*.csv".format(odir))
    for old_report in old_reports:
        os.system("mv {} {}/old_reports".format(old_report, odir))
    for old_shipment in old_shipments:
        os.system("mv {} {}/old_reports".format(old_shipment, odir))

    info_file = "{}/Informe_General_{}.md".format(odir, compact_date)
    shipments_csv = "{}/Shipments_{}.csv".format(odir, compact_date)
    shipfile = open(shipments_csv, "w") # Añadido a posteriori porque Manoli quiere la tabla en CSV
    shipfile.write("Hospital;Envíos;Muestras;En_proceso;Procesadas;Seq;Falla_QC;Falla_Lab\n")
    with open(info_file, "w") as outfile:
        informe = "# Informe de seguimiento SeqCOVID-SPAIN\n" \
        + "\n" \
        + "**Fecha de actualización:** {:02d}/{:02d}/{} a las {}:{}\n".format(now.day,
                                                             now.month,
                                                             now.year,
                                                             now.hour,
                                                             now.minute) \
        + "\nEn el marco del proyecto SeqCOVID-SPAIN, hemos recibido " \
        + "un total de {} muestras.".format(summary["grand_total"]["total"])\
        + " De estas, {} han sido procesadas ".format(summary["grand_total"]["processed"])\
        + "y {} ({}%) han sido secuenciadas con éxito".format(
                                              summary["grand_total"]["success_seq"],
                                              summary["grand_total"]["success_rate"])\
        + " según el criterio de calidad mínimo establecido\n\n" \
        + "La siguiente tabla resume la información sobre las muestras procesadas"\
        + " para cada hospital.\n" \
        + "\n __Los envíos pendientes están marcados con un asterisco__" \
        + "\n| Hospital | Envíos | Muestras | En proceso | Procesadas | Seq | Falla QC | Falla Lab |\n" \
        + "| -------- | ------ | ---- | ---- | ---- | ---- | ---- | ---- |\n"
        for hospital in SHIPMENTS:
            nsamples      = 0
            processed     = 0
            in_progress   = 0
            sequenced     = 0
            failed_qc     = 0
            failed_lab    = 0
            ship_received = []
            ship_pending  = []
            hospcode      = hospital.split("(")[1].rstrip(")")
            for shipcode in SHIPMENTS[hospital]:
                nsamples    += SHIPMENTS[hospital][shipcode]["num_samples"]
                in_progress += SHIPMENTS[hospital][shipcode]["in_progress"]
                processed   += SHIPMENTS[hospital][shipcode]["processed"]
                sequenced   += SHIPMENTS[hospital][shipcode]["sequenced"]
                failed_qc   += SHIPMENTS[hospital][shipcode]["failed_qc"]
                failed_lab  += SHIPMENTS[hospital][shipcode]["failed_lab"]
                if not SHIPMENTS[hospital][shipcode]["received"]:
                    ship_pending.append(shipcode + "*") # asterisk to know is pending
                else:
                    ship_received.append(shipcode)
            shipments = ship_received + ship_pending
            shipments = ", ".join(shipments)
            informe += "| {} | {} | {} | {} | {} | {} | {} | {} |\n".format(
            hospital.split("(")[0].replace("_", " "),
            shipments,
            nsamples,
            in_progress,
            processed,
            sequenced,
            failed_qc,
            failed_lab
            )
            shipfile.write("{};{};{};{};{};{};{};{}\n".format(
            hospital.split("(")[0].replace("_", " "),
            shipments,
            nsamples,
            in_progress,
            processed,
            sequenced,
            failed_qc,
            failed_lab
            ))
        outfile.write(informe)
        shipfile.close()
        informe = "\n\n" + \
                  "## Muestras procesadas para cada hospital en función de la fecha\n" + \
                  "\nIMAGE_SAMPLES" # IMAGE_SAMPLES string will be replaced within the
                                  # html to embed the actual plot

        informe += "\n\n" + \
                  "## Relación entre Ct (qPCR) Ct y coverage genómico\n" + \
                  "\nIMAGE_QPCR"

        informe += "\n\n" + \
                  "## Linajes de las muestras según pangolin (Rambaut _et al._)\n" + \
                  "\nIMAGE_LINAJES"

        outfile.write(informe)

    # CONVERT REPORT TO HTML
    prefix = info_file.split(".")[0]

    os.system("/usr/bin/grip --user=seqcovid --pass=106fec64516f473507fddafd0d8687938cedf384 --export {}.md {}.html".format(prefix, prefix))
    os.system("rm {}.md".format(prefix))
    #Embed txt imges in html
    embed_html_images(args, prefix)

    return 0


def clean_informes_dir(DB, args):
    
    odir = os.path.abspath(args.odir)

    for hospital in DB:

        # Get the Hospital name formatted for dir output
        hospformat = get_hospital_dirname(hospital)
        
        # Get the hospital full directory name and create it if not exist
        hosp_dir = "{}/{}".format(odir, hospformat)

        # Create the folder if does not exists
        if not os.path.exists(hosp_dir):
            os.mkdir(hosp_dir)        
        
        os.system("mv {}/Informe_{}.html {}/{}/".format(odir, hospformat,
                                                        odir, hospformat))
        os.system("mv {}/plot_data/{}.datos_muestras.csv {}/{}/".format(
                   odir, hospformat,
                   odir, hospformat))
        
        # Get the txt filepath and try to delete it 
        txt_path = "{}/plot_data/{}*.txt".format(odir, hospformat)
        if os.path.exists(txt_path):
            os.remove(txt_path)

        ###################################
        # Create the fasta for the hospital
        ################################### 
        csv_path   = "{}/{}/{}.datos_muestras.csv".format(odir, hospformat, hospformat)
        fasta_path = "{}/{}/{}.consensus.fasta".format(odir, hospformat, hospformat)

        # Create the full-fasta file for the hospital
        f1 = open(fasta_path, 'w') 
        
        # For each sample in such hospital...
        for cov in DB[hospital]:

            # Jump not sequenced samples
            status = DB[hospital][cov]["status"]
            if not "sequenced" in status.lower():
                continue

            # Get the file info
            seq_place = DB[hospital][cov]["seq_place"]
            run_code  = DB[hospital][cov]["run_code"]
            seq_tech  = DB[hospital][cov]["seq_tech"]
            consensus_file = get_consensus_file(cov, seq_place, run_code, seq_tech)

            # Append COV consensus
            if os.path.exists(consensus_file):
                f2 = open(consensus_file, 'r')
                f1.write(f2.read())
                # New line??
                f2.close()

        # Close full fasta file
        f1.close()

        
        '''
        OLD WAY TO CREATE THE MULTIFASTA
        # Create multifasta consensus for all samples from that hospital
        os.system("grep sequenced {}/{}/{}.datos_muestras.csv | cut -f1 -d';' |".format(odir, hospformat, hospformat)
                + " while read COV; do" \
                + " grep ${COV} /data2/collaborations/COVID-19_data_transfers/seqcovid/data/data_mirror/analysis_files.path" \
                + " | awk -v FS=\";\" '$2 == \"consensus\"'; done" \
                + " | cut -f3 -d';' | while read CONSPATH; do" \
                + " cat /data2/collaborations/COVID-19_data_transfers/seqcovid/data/data_mirror/${CONSPATH}; done" \
                + " > {}/{}/{}.consensus.fasta".format(odir, hospformat, hospformat)
                )
        '''

    return 0


def main():

    args = parse_args()
    DB, SHIPMENTS = read_DB()
    summary = summarize_DB(DB)
    generate_plots(DB, args)
    per_hospital_report(DB, SHIPMENTS, summary, args)
    grand_total_report(summary, SHIPMENTS, args)
    clean_informes_dir(DB, args)

    return 0


if __name__ == '__main__':
    main()
