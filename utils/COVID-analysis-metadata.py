#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Write metadata files for GISAID, ENA and Microreat 
data uploading within the SeqCOVID project
'''
#---------------------------------------------------------------
__author__      = 'Galo A. Goig, Santiago Jiménez-Serrano'
__credits__     = ['Galo A. Goig', 'Santiago Jiménez-Serrano']
__copyright__   = 'Copyright 2020, SeqCOVID-Spain Consortium'
__email__       = 'bioinfo.covid19@ibv.csic.es'
#---------------------------------------------------------------


import os
import sys
import argparse
import COVID_Utils          as cutils
import COVID_SpainMap_Utils as sp_map_utils
import COVID_SQL_Queries    as dbqueries


def parse_args():
    '''
    Parse arguments given to script
    '''    

    parser = argparse.ArgumentParser(description='Write metadata files for'
    ' GISAID, ENA and Microreat data upload within the SeqCOVID project')
    parser.add_argument('-m', '--master_table',     dest='master_table', metavar='MASTER TABLE', required=True)
    parser.add_argument('-o', '--output-directory', dest='odir', required=True)
    parser.add_argument('-r', '--run-directory',    dest='run_dir', required=True,
        help=('Folder where the anaysis run has been performed. From this folder '
        'SARS-CoV-2 lineages and other data is read to generate Microreact metadata'))
    parser.add_argument('--run-id', dest='runid', required=True)
    parser.add_argument("-silent", dest="silent", 
        type=cutils.str2bool, 
        nargs='?', const=True, default=False, help="Specify silent mode")
    args = parser.parse_args()

    return args



def parse_authors_list():    

    # Get path of the running script & replace utils by 'Authors_list.csv'
    utils       = os.path.dirname(os.path.realpath(__file__))
    author_list = utils.replace('utils', 'Authors_list.csv')
    AUTHORS     = {}    
    with open(author_list) as infile:
        infile.readline() # Skip header
        for line in infile:
            line = line.rstrip().split(';')
            hosp_DB, institution, authors = line
            AUTHORS[hosp_DB] = (institution, authors)

    return AUTHORS



def get_sample_coordinates(cov_record):
    '''
    Gets the sample coordinates for a given a cov record checking nulls
    If no sample coordinates exists, return the hospital ones
    '''

    # Get the coordinates
    lat = cov_record['sample_latitude']
    lon = cov_record['sample_longitude']

    # Set the hospital coordinates if no sample location is set in the DB
    if not lat or not lon:
        lat = cov_record['latitude']
        lon = cov_record['longitude']

    return lat, lon


def Load_COVS(args):
    '''
    Loads the list of COVS to retrieve the info from the run master_table
    '''    

    mtable_path = os.path.abspath(args.master_table)
    mtable_dir  = os.path.dirname(mtable_path)
    covs = {}

    with open(args.master_table) as infile:        
        for line in infile:
            cov    = line.split(';')[0].rstrip('m') # m just in case is resequencing
            fastq1 = '{}/{}.R1.humanfilt.fastq.gz'.format(mtable_dir, cov)
            fastq2 = '{}/{}.R2.humanfilt.fastq.gz'.format(mtable_dir, cov)

            # Try avoiding non COV samples like negatives
            if cov.startswith('COV'): 
                covs[cov] = (fastq1, fastq2)
    return covs


def parse_QC_summary(args):
    '''
    Parses QC_summary to get coverage, depth and lineage
    '''
    
    # Path is to get absolute and normalized PATH for the output directory
    rundir = os.path.abspath(args.run_dir)
    if not os.path.exists(rundir):
        sys.exit('Run directory does not exist\n')
    
    # Get the QC_summary file path
    QC_summary = '{}/reports/QC_summary.{}.csv'.format(rundir, args.runid)
    if not os.path.exists(QC_summary):
        sys.exit('QC Summary could not be found: {}\n'.format(QC_summary))

    # Other default values - Could by modified by the user
    seq_tech    = 'Illumina MiSeq'
    seq_library = 'ARTICV3 + Nextera Flex'

    qc_dict = {}

    with open(QC_summary) as infile:

        # Skip header
        infile.readline()

        for line in infile:
            tokens       = line.rstrip().split(';')
            cov          = tokens[0]
            runid        = tokens[1]
            seq_place    = tokens[2]
            mean_depth   = tokens[3]
            median_depth = tokens[4]
            coverage     = float(tokens[5])
            ambiguous    = int(tokens[9])
            nsnps        = int(tokens[10])
            lineage      = tokens[13]
            clades       = tokens[14]
            mutations    = tokens[15]
            psarscov2    = tokens[23]
            phomosapiens = tokens[24]

            if coverage >= 0.9:
                status = 'Sequenced'
            else:
                status = 'Failed_QC'

            qc_dict[cov] = {
                'cov'          : cov,
                'runid'        : runid,
                'mean_depth'   : mean_depth,
                'median_depth' : median_depth,
                'coverage'     : coverage,
                'ambiguous'    : ambiguous,
                'nsnps'        : nsnps,
                'lineage'      : lineage,
                'clades'       : clades,
                'mutations'    : mutations,
                'status'       : status,
                'seq_place'    : seq_place,
                'seq_tech'     : seq_tech,
                'seq_library'  : seq_library,
                'p_sarscov2'   : psarscov2,
                'p_homosapiens': phomosapiens
            }

    return qc_dict



def parse_SQL_info(args, SQL_info, qc_dict):
    '''
    Given the data returned by the SQL query, parse it, clean it
    and store into a dict
    '''

    '''
    # COLUMN  0 => sample.gId
    # COLUMN  1 => sample.hospital_date
    # COLUMN  2 => sample.residence_city
    # COLUMN  3 => sample.residence_province
    # COLUMN  4 => sample.residence_country
    # COLUMN  5 => sample.gender
    # COLUMN  6 => sample.age
    # COLUMN  7 => sample.seq_tech
    # COLUMN  8 => hospital.name
    # COLUMN  9 => hospital.address
    # COLUMN 10 => sample.lineage
    # COLUMN 11 => sample.coverage
    # COLUMN 12 => sample.median_depth
    # COLUMN 13 => hospital.latitude
    # COLUMN 14 => hospital.longitude
    # COLUMN 15 => sample.sip
    # COLUMN 16 => sample.sample_latitude
    # COLUMN 17 => sample.sample_longitude
    # COLUMN 18 => sample.hospital_sample_number
    '''

    COV_info = {}
    for record in SQL_info:        
        cov          = cutils.number2cov(record[0])        
        dt, yy, mm, \
            dd, epiw = cutils.getDetailedDate(record[1], 'unknown')
        city         = record[2].strip().replace(' ', '_')
        province     = record[3].strip().replace(' ', '_')
        country      = record[4].strip().replace(' ', '_')
        gender       = cutils.getSafeGender(record[5])
        age          = cutils.getSafeAge(   record[6])
        seq_tech     = record[7].replace(' ', '_')
        ori_lab      = record[8].strip().replace(' ', '_')
        ori_address  = record[9].strip()
        lineage      = record[10].strip()
        branch       = cutils.getTreeBranch(lineage)
        coverage     = record[11]
        median_depth = record[12]
        latitude     = record[13]
        longitude    = record[14]
        patient      = record[15]
        sample_latitude  = record[16]
        sample_longitude = record[17]
        hospital_sample_number= record[18]

        # Get the sample origin Province & City from the Hospital Address
        ori_PROV = ori_address.split(',')[-2].strip().replace(' ', '_')
        ori_CITY = ori_address.split(',')[-3].strip().replace(' ', '_')
        ori_ccaa = sp_map_utils.get_CCAA(ori_PROV)

        # Si no se conoce la ciudad, provincia, pais,
        # se le pone la del hospital mas una _h para saberlo
        if not city:
            city = '{}_h'.format(ori_CITY)

        # Ensure some value for the province
        if not province or not sp_map_utils.exist_spain_province(province):
            province = ori_PROV

        # Set always Spain as origin country
        if country != 'Spain':
            if not args.silent:
                WARN = 'Warning [{}]: => '.format(cov)        \
                    + 'Origin country is {} '.format(country) \
                    + 'but it has been set to default (Spain)\n'
                sys.stderr.write(WARN)
            country = 'Spain'
            
        
        # Get the full CCAA name
        ccaa = sp_map_utils.get_CCAA(province)

        # Get the patient residence location
        location = 'Europe/{}/{}/{}'.format(country, ccaa, city)

        # Get the origin location (hospital)
        hospital_location = 'Europe/Spain/{}/{}'.format(ori_ccaa, ori_CITY)

        # Get the COVxxxxxx number, (6-zero padded)
        cov_number = str(record[0]).rjust(6, '0')

        # Hospital's Province ==> CCAA code
        ccaa_code  = sp_map_utils.get_CCAA_ISO3166(ori_PROV)

        # Seq Place GISAID code
        gcode = cutils.gisaid_originlab_code(qc_dict[cov]['seq_place'])

        # The virus-name's Identifier field (GISAID)
        identifier = ccaa_code + '-IBV-' + gcode + cov_number

        # Set to 2020 the year if unknown (GISAID)
        gyear = yy
        if yy == 'unknown':
            gyear = '2020'

        # Set Virus Name according to GISAID standards
        #  hCoV-19/Country/Identifier/year of collection
        virus_name = 'hCoV-19/Spain/{}/{}'.format(identifier, gyear)

        
        # Create the record in the output dictionary
        COV_info[cov] = {
            'hosp_date'         : dt,
            'year'              : yy,
            'month'             : mm,
            'day'               : dd,
            'city'              : city,
            'province'          : province,
            'country'           : country,
            'location'          : location,
            'hospital_location' : hospital_location,
            'gender'            : gender,
            'age'               : age,
            'seq_tech'          : seq_tech,
            'ori_lab'           : ori_lab,
            'ori_address'       : ori_address,
            'lineage'           : lineage,
            'branch'            : branch,
            'coverage'          : coverage,
            'median_depth'      : median_depth,
            'virus_name'        : virus_name,
            'latitude'          : latitude,
            'longitude'         : longitude,
            'patient'           : patient,
            'sample_latitude'   : sample_latitude,
            'sample_longitude'  : sample_longitude,
            'hospital_sample_number' : hospital_sample_number
            }

    return COV_info



def SQL_query(args, covs):
    ''' 
    Connect to the DB, retrieve info for a given list of COVS
    and return that info into a dictionary
    '''

    # quito el COV y lo paso a int para quitar los 0s de delante ya que los ids
    # en la BD internamente están como numeros
    ids = tuple(map(lambda x: int(str.replace(x,'COV','')),covs))

    # In case there is only one cov, it must be a string instead of a
    # tuple for the SQL query
    comparison_operator = 'IN '
    if len(ids) == 1:
        ids = ids[0]
        comparison_operator = '= '


    # Get the SQL sentence
    sql = \
        'SELECT '                   + \
        's.gId, '                   + \
        's.hospital_date, '         + \
        's.residence_city, '        + \
        's.residence_province, '    + \
        's.residence_country, '     + \
        's.gender, '                + \
        's.age, '                   + \
        's.seq_tech, '              + \
        'h.name, '                  + \
        'h.address, '               + \
        's.lineage, '               + \
        's.coverage, '              + \
        's.median_depth, '          + \
        'h.latitude, '              + \
        'h.longitude, '             + \
        's.sip, '                   + \
        's.sample_latitude, '       + \
        's.sample_longitude, '      + \
        's.hospital_sample_number ' + \
        'FROM sample s INNER JOIN hospital h ' + \
        ' ON s.hospital_id = h.id' + \
        ' WHERE s.gId ' + comparison_operator + str(ids)
                     

    # Run the sql query
    SQL_info = dbqueries.Execute_SELECT(sql)
    
    # Returns the results set
    return SQL_info



def write_GISAID_metadata(args, COV_info, qc_dict):
    '''
    Function to generate the row with the info to submit to GISAID
    '''
    
    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir = os.path.abspath(args.odir)

    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)

    runid = args.runid
    AUTHORS = parse_authors_list()
    header_GISAID =          \
        'Submitter@'       + \
        'FASTA filename@'  + \
        'Virus name@'      + \
        'Type@'            + \
        'Passage details/history@'+\
        'Collection date@' + \
        'Location@'        + \
        'Additional location information@' + \
        'Host@'            + \
        'Additional host information@' + \
        'Gender@'          + \
        'Patient age@'     + \
        'Patient status@'  + \
        'Specimen source@' + \
        'Outbreak@'        + \
        'Last vaccinated@' + \
        'Treatment@'       + \
        'Sequencing technology@' + \
        'Assembly method@' + \
        'Coverage@'        + \
        'Originating lab@' + \
        'Address@'         + \
        'Sample ID given by the sample provider@' + \
        'Submitting lab@'  + \
        'Address@'         + \
        'Sample ID given by the submitting laboratory@' + \
        'Authors@'         + \
        'Comment@'         + \
        'Comment Icon\n'

    with open('{}/GISAID_{}_metadata.csv'.format(odir, runid), 'w') as outfile:

        # El archivo de GISAID se llamara, e.g: GISAID_200514_MO01239_metadata.csv
        # donde args.runid == 200514_MO01239
        outfile.write(header_GISAID)

        for cov in COV_info:
            hosp_DB = COV_info[cov]['ori_lab']
            if hosp_DB in AUTHORS:
                institution, authors = AUTHORS[hosp_DB]
            else:
                institution = hosp_DB
                authors = 'AUTHORS'
            
            # Append the SeqCOVID suffix to authors
            authors = '{} and SeqCOVID-SPAIN consortium'.format(authors)
            address        = 'Jaume Roig St, 11. Valencia. E-46010. Spain'
            submitting_lab = 'SeqCOVID-SPAIN consortium/IBV(CSIC)'
            fasta_filename = 'GISAID_consensus.{}.fasta'.format(runid)

            # In GISAID, the sample location must be the hospital one
            location               = COV_info[cov]['hospital_location']
            additional_location    = 'Patient residence: {}'.format(COV_info[cov]['location'])
            hospital_sample_number = COV_info[cov]['hospital_sample_number']

            GISAID_row = '{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}\n'.format(
            'seqcovid',                  # Submitter
            fasta_filename,              # Fasta filename
            COV_info[cov]['virus_name'], # Virus name
            'betacoronavirus',           # Type
            'Original',                  # Passage details/history
            COV_info[cov]['hosp_date'],  # Collection date
            location,                    # Location
            additional_location,         # Additional_location_info
            'Human',                     # Host
            '',                          # Additional_host_info
            COV_info[cov]['gender'],     # Gender
            COV_info[cov]['age'],        # Age
            'unknown',                   # Patient status
            '',                          # Specimen source
            '',                          # Outbreak
            '',                          # Last_vaccinated
            '',                          # Treatment,
            COV_info[cov]['seq_tech'],   # Sequencing technology
            'BWA/IVAR',                  # Assembly_method
            qc_dict[cov]['mean_depth'],  # mean_depth, GISAID says 'coverage'
            institution,                 # Originating laboratory
            COV_info[cov]['ori_address'],# Ori Lab Address
            hospital_sample_number,      # Sample ID from provider
            submitting_lab,              # Submitting Lab
            address,                     # Address
            cov,                         # Sample ID from submitter
            authors,                     # Authors
            '',                          # Comments
            ''                           # Icon
            )

            # Only write for genomes that pass QC
            if qc_dict[cov]['coverage'] >= 0.9:
                outfile.write(GISAID_row)
        return 0



def write_SeqCOVID_DB(args, COV_info, qc_dict):
    '''
    Generate a CSV to update our DB
    '''    
    
    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir = os.path.abspath(args.odir)
    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)
    
    runid  = args.runid
    covs   = Load_COVS(args)
    header = 'ID,Status,Coverage,Median depth,Lineage,Run code,Sequencing place,Sequencing technology,Sequencing library,Clades,Mutations,Percent SarsCov2,Percent HomoSapiens\n'

    # Open the DB_UPDATE.csv file for writting
    with open('{}/DB_UPDATE_{}.csv'.format(odir, runid), 'w') as outfile:

        # Write the header
        outfile.write(header)

        # Write a row for each cov
        for cov in covs:
            line = '{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(
            cov,
            qc_dict[cov]['status'],
            qc_dict[cov]['coverage'],
            qc_dict[cov]['median_depth'],            
            qc_dict[cov]['lineage'],
            runid,
            qc_dict[cov]['seq_place'],
            qc_dict[cov]['seq_tech'],
            qc_dict[cov]['seq_library'],
            qc_dict[cov]['clades'],
            qc_dict[cov]['mutations'],
            qc_dict[cov]['p_sarscov2'],
            qc_dict[cov]['p_homosapiens']
            )
            outfile.write(line)
    return 0



def write_ENA_manifest(cov, manifest_dir, covs, qc_dict):
    fastq1, fastq2 = covs[cov]
    coverage  = qc_dict[cov]['coverage']
    #ambiguous = qc_dict[cov]['ambiguous']
    #nsnps     = qc_dict[cov]['nsnps']
    # HQ genomes are those with more than 90% genomic coverage
    # and max mean + 2 times std dev ambiguous nts and SNPs
    # now that we have sequenced almost 2K strains these values are:
    # max 11 ambiguous nts and max 21 SNPs
    #if coverage >= 0.9 and (ambiguous <= 11 and nsnps <= 21):
    # NOTE: Removed ambiguous and nsnps condition
    if coverage >= 0.9:
        manifest = '{}/{}.HiQ.ENA_manifest'.format(manifest_dir, cov)
    else:
        manifest = '{}/{}.LoQ.ENA_manifest'.format(manifest_dir, cov)

    with open(manifest, 'w') as outfile:
        outfile.write('STUDY\tPRJEB37513\n')
        outfile.write('SAMPLE\t{}\n'.format(cov))
        outfile.write('NAME\t{}\n'.format(cov))
        outfile.write('FASTQ\t{}\n'.format(fastq1))
        outfile.write('FASTQ\t{}\n'.format(fastq2))
        outfile.write('INSTRUMENT\tIllumina MiSeq\n')
        outfile.write('LIBRARY_SOURCE\tVIRAL RNA\n')
        outfile.write('LIBRARY_SELECTION\tPCR\n')
        outfile.write('LIBRARY_STRATEGY\tWGA\n')
        outfile.write('DESCRIPTION\tWGA of SARS-CoV-2 amplified using ARTIC V3 protocol\n')
    return 0



def write_ENA_checklist(args, COV_info, covs, qc_dict):
    '''
    Function to generate the row with the info to submit to ENA
    '''

    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir = os.path.abspath(args.odir)
    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)

    runid   = args.runid
    AUTHORS = parse_authors_list()
    
    # Create dir for ENA manifest files
    manifest_dir = '{}/ENA_manifests'.format(odir)
    if not os.path.exists(manifest_dir):
        os.mkdir(manifest_dir)

    # Get the ENA header
    ENA_HEADER  = '#checklist_accession\tERC000033\n'
    ENA_HEADER += '#unique_name_prefix\tSeqCOVID-Spain\n'
    ENA_HEADER += 'sample_alias\ttax_id\tscientific_name\tcommon_name\t'
    ENA_HEADER += 'sample_title\tsample_description\tcollection date\t'
    ENA_HEADER += 'geographic location (country and/or sea)\t'
    ENA_HEADER += 'geographic location (latitude)\t'
    ENA_HEADER += 'geographic location (longitude)\t'
    ENA_HEADER += 'geographic location (region and locality)\t'
    ENA_HEADER += 'host common name\thost subject id\thost age\t'
    ENA_HEADER += 'host health state\thost sex\t'
    ENA_HEADER += 'host scientific name\tvirus identifier\tcollector name\t'
    ENA_HEADER += 'collecting institution\tisolate\n'
    ENA_HEADER += '#template\n'
    ENA_HEADER += '#units\t\t\t\t\t\t\t\tDD\tDD\t\t\t\tyears\n'

    # Open the output file
    with open('{}/ENA_{}_metadata.csv'.format(odir, runid), 'w') as outfile:
        outfile.write(ENA_HEADER)

        # For each COV
        for cov in COV_info:

            coverage  = qc_dict[cov]['coverage']
            #ambiguous = qc_dict[cov]['ambiguous']
            #nsnps     = qc_dict[cov]['nsnps']

            # HQ genomes are those with more than 90% genomic coverage
            # and max mean + 2 times std dev ambiguous nts and SNPs
            # now that we have sequenced almost 2K strains these values are:
            # max 11 ambiguous nts and max 21 SNPs
            #if coverage >= 0.9 and (ambiguous <= 11 and nsnps <= 21):
            # NOTE: Removed ambiguous and nsnps condition
            if coverage >= 0.9:

                # Get the institution and authors safely
                hosp_DB = COV_info[cov]['ori_lab']
                if hosp_DB in AUTHORS:
                    institution, authors = AUTHORS[hosp_DB]
                else:
                    institution = hosp_DB
                    authors     = 'AUTHORS'

                # Append the SeqCOVID consortium to authors
                authors     = '{} and SeqCOVID-Spain consortium'.format(authors)
                institution = '{} and SeqCOVID-Spain consortium'.format(institution)

                # Get the gender safely
                gender = COV_info[cov]['gender']
                if gender == 'unknown':
                    gender = 'not provided'

                # Get the age safely
                age = COV_info[cov]['age']
                if age == 'unknown':
                    age = '99999'
                
                # Large constant values
                scientific_name = 'Severe acute respiratory syndrome coronavirus 2'
                sample_desc     = 'WGA sequencing of SARS-CoV-2 samples'
                
                # Get the sample coordinates (hospital ones if not exist)
                latitude, longitude = get_sample_coordinates(COV_info[cov])


                ENA_row = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(
                cov,
                '2697049',                          # TAXID
                scientific_name,                    # SCIENTIFIC NAME
                'SARS-CoV-2',                       # COMMON NAME
                'SARS-CoV-2 genome {}'.format(cov), # SAMPLE_TITLE ??
                sample_desc,                        # SAMPLE DESCRIPTION
                COV_info[cov]['hosp_date'],         # COLLECTION DATE
                'Spain',                            # GEOG Location, Country
                latitude,                           # Sample location latitude
                longitude,                          # Sample location longitude
                COV_info[cov]['city'],              # geog location region and locality
                'Human',                            # host common name
                COV_info[cov]['patient'],           # host subject id
                age,
                'not provided',                     # host health state
                gender.lower(),
                'Homo sapiens',                     # host scientific name
                cov,                                # virus identifier
                authors,                            # collector name
                institution,                        # collecting institution,
                cov,                                # isolate
                COV_info[cov]['ori_lab'],           # para tener la referencia de que autores poner
                )
                outfile.write(ENA_row)
            
            # write also the file used to upload the sample to ENA
            write_ENA_manifest(cov, manifest_dir, covs, qc_dict)
    return 0



def main():
    
    # Load arguments
    args = parse_args()

    # Read the master table
    covs = Load_COVS(args)

    # Read the QC Summary
    qc_dict = parse_QC_summary(args)

    # Read the DB
    SQL_info = SQL_query(args, covs)

    # Pass SQL_info to a dictionary
    COV_info = parse_SQL_info(args, SQL_info, qc_dict)
    
    # Write the metadata files
    write_GISAID_metadata(args, COV_info, qc_dict)
    write_SeqCOVID_DB(args, COV_info, qc_dict)
    write_ENA_checklist(args, COV_info, covs, qc_dict)

    return 0



if __name__ == '__main__':
    main()
