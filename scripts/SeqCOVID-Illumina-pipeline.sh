#!/usr/bin/bash
#################################################################################
## SeqCOVID-Illumina-pipeline.sh 
## 
## Description:
##    Analyze Sars-Cov-2 data within the frame of the 
##    SeqCOVID-Spain consortium: Sars-Cov-2 sequencig project
##
##    For any question please visit:
##    https://gitlab.com/fisabio-ngs/sars-cov2-mapping
##
##
## Unidad de Análisis Computacional COVID19 / Unidad de Genómica de Tuberculosis
## Instituto de Biomedicina de Valencia
## Calle Jaume Roig, 11 (46010)
## València, España
##
## Fundación para el Fomento de la Investigación Sanitaria y 
## Biomédica de la Comunitat Valenciana (FISABIO)
## Servicio de Secuenciación y Bioinformática
## Av. Catalunya, 21 (46020)
## València, España
##
## __author__    = Galo A. Goig
## __author__    = Giuseppe D'Auria
## __author__    = Santiago Jiménez-Serrano
## __copyright__ = Copyright 2020, SeqCOVID-Spain Consortium
## __email__     = bioinfo.covid19@ibv.csic.es
## __date__      = 2020 April
## __version__   = 1.0.2
##
################################################################################

################################################################################
#                                                           FUNCTION DEFINITIONS
################################################################################
# First block: Function definitions that will run during the analysis

################################################################################
# Function of usage message and help
################################################################################
usage () 
{
	echo "$(basename $0)"
	echo "Usage:"
	echo "  -h                 Display this help message"
	echo "  -c  CONFIG_FILE    Provide a configuration file"
	echo "  -m  MASTER_TABLE   Table with sample names and respective fastq files"
	echo "  -f  FASTQFOLDER    Folder where fastq files and MASTER_TABLE are"
	echo "                     located. (Where this script will read data from)"
	echo "  -r  RUNID"
	echo "  -o  OUTFOLDER      Folder where the analysis will be run and where"
	echo "                     the results will be stored. OUTFOLDER should match the RUN ID."
	echo "  -t  THREADS        Number of threads to be used by multithreading-compatible software"
	echo "  -k  KRAKEN_VERSION Either provide kraken or kraken2"
	echo ""
	exit 1
}

################################################################################
# Log functions
################################################################################
# Print and log messages in LOG_FILE
log_string() 
{
    echo -e "$@" | tee -a ${LOG_FILE}
}

# Run a command and log any message printed to stdout in LOG_FILE
log_command() 
{
    echo -e "$@" |  tee -a ${LOG_FILE}
    echo "$@" | bash 2>>${LOG_FILE} | tee -a ${LOG_FILE}
}

################################################################################
# FastQC Quality 
################################################################################
fastqc() 
{
	# SAMPLE R1 and R2 are passed to fastp_trim as read below from master_table
	# with a while loop
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Quality assessment using FastQC"
	log_string "-----------------------------------------------------------------"
	log_command "fastqc ${FF_REAL}/*fastq.gz -o reports/fastqc " \
                " --threads ${THREADS} --quiet "
	log_string ""
}

################################################################################
# Kraken Filtering
################################################################################
kraken_filter()
{
	# Removal of reads classified as Homo Sapiens (taxid: 9606) by Kraken.

	SAMPLE=${1}
	R1=${2}
	R2=${3}
	THREADS=${4}
	KRAKEN_V=${5}

	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Removal of human reads with kraken: ${SAMPLE}            "
	log_string "-----------------------------------------------------------------"
	# First taxonomically classify the reads
	if [ "${KRAKEN_V}" == 'kraken2' ]
	then
		log_command "kraken2"  \
			" --db ${KRAKEN_DB}"      \
			" --paired ${R1} ${R2}"                 \
			" --output kraken/${SAMPLE}.kraken" \
			" --threads ${THREADS} --gzip-compressed"
	else
	        # First taxonomically classify the reads
        	log_command "kraken "  \
                	" --db ${KRAKEN_DB} --fastq-input "      \
                	" --paired ${R1} ${R2} "                 \
                	" --output kraken/${SAMPLE}.kraken " \
                	" --threads ${THREADS} --gzip-compressed "
	fi

	# Get the identifiers of reads that belong to human (taxid: 9606)
	awk '$3 == "9606" { print $2 }' kraken/${SAMPLE}.kraken > \
		kraken/${SAMPLE}.kraken.human.ids
	
	# Pick reads from a fastq given a list of readids using ad-hoc script (R1 & R2)
	log_command "python ${SCRIPT_DIR}/../utils/Script-fasta-pick-readlist.py -in ${R1}" \
                " -r kraken/${SAMPLE}.kraken.human.ids -a discard "          \
                " -o kraken/${SAMPLE}.R1.humanfilt.fastq -f fastq -c"
	
	log_command "python ${SCRIPT_DIR}/../utils/Script-fasta-pick-readlist.py -in ${R2}" \
                " -r kraken/${SAMPLE}.kraken.human.ids -a discard "          \
                " -o kraken/${SAMPLE}.R2.humanfilt.fastq -f fastq -c"

	# Gzip R1 & R2 fastq files
	log_command "gzip -f kraken/${SAMPLE}.R1.humanfilt.fastq"
	log_command "gzip -f kraken/${SAMPLE}.R2.humanfilt.fastq"
}

################################################################################
# Generate Kraken reports based on .kraken files
################################################################################
kraken_report()
{
	SAMPLE=${1}
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Generating kraken report                          "
	log_string "-----------------------------------------------------------------"
	log_command "kraken-report --db ${KRAKEN_DB} " \
	            " kraken/${SAMPLE}.kraken > reports/${SAMPLE}.report"
	# I will take advantage of ThePipeline contaminants function to extract
	# ready-to-plot data
	log_command "ThePipeline contaminants -p reports/${SAMPLE}"
	log_command "gzip -f kraken/${SAMPLE}.kraken"
}

################################################################################
# FastP Quality  trimming
################################################################################
fastp_trim () 
{
	# SAMPLE R1 and R2 are passed to fastp_trim as read below from master_table
	# with a while loop
	SAMPLE=$1
	NEXTERAADAPT=${SCRIPT_DIR}/../IlluminaAdaptors.fasta
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Quality trimming using fastp: ${SAMPLE}"
	log_string "-----------------------------------------------------------------"
	log_command "fastp " \
                " --in1 kraken/${SAMPLE}.R1.humanfilt.fastq.gz "        \
                " --in2 kraken/${SAMPLE}.R2.humanfilt.fastq.gz "        \
                " --out1 trimm/${SAMPLE}.R1.trim.fastq.gz "                       \
                " --out2 trimm/${SAMPLE}.R2.trim.fastq.gz "                       \
                " --detect_adapter_for_pe "                                 \
                " --adapter_fasta ${NEXTERAADAPT} "                         \
                " --cut_tail "                                              \
                " --cut_window_size ${WIN_SIZE} "                           \
                " --cut_mean_quality ${MEAN_QUAL} "                         \
                " --thread ${THREADS} "                                     \
                " --max_len1 ${MAX_LEN1} "                                  \
                " --max_len2 ${MAX_LEN2} "                                  \
                " --json reports/${SAMPLE}_fastp.json "                     \
                " --html reports/${SAMPLE}_fastp.html "
	log_string ""
}

################################################################################
# IVAR VERSION
################################################################################
# ivar is under active development and it is ** VERY RELEVANT ** to use proper
# version. So we will log which version is the pipeline using to track possible
# errors with version changes and conda environments
ivar_version () 
{
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Ivar Version"
	log_string "-----------------------------------------------------------------"
	log_command "ivar version"
	log_string ""
}

################################################################################
# Map reads to ref genome with BWA
################################################################################
mapping () 
{
	SAMPLE=$1

	#-------- Running Mapping
        log_string ""
        log_string "-----------------------------------------------------------------"
        log_string "    Index reference genome"
        log_string "-----------------------------------------------------------------"
	log_command "bwa index ${REF_GENOME}" 
	log_string ""

        log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "	Mapping reads to reference genome"
	log_string "-----------------------------------------------------------------"
	log_command "bwa " \
                " mem -Y -M -R '@RG\tID:.\tSM:.' -t ${THREADS} ${REF_GENOME}" \
                " trimm/${SAMPLE}.R1.trim.fastq.gz " \
                " trimm/${SAMPLE}.R2.trim.fastq.gz " \
                " | samtools sort " \
                " | samtools view -F 4 -b -@ ${THREADS} -o map/${SAMPLE}.sort.bam"
	log_string ""
	# First mapping indexing
	log_string "--- Index trimmed bam"
	log_command "samtools index map/${SAMPLE}.sort.bam"
	log_string ""
}

################################################################################
# Trim/mask primers with ivar
################################################################################
ivar_trim () 
{
	SAMPLE=$1
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Primer trimming/masking with ivar"
	log_string "-----------------------------------------------------------------"
	log_command "ivar trim -i map/${SAMPLE}.sort.bam -p map/${SAMPLE}.trim"    \
                " -m ${MINTRIM_LENGTH} -q ${MINQUALTRIM} -s ${WINDOW}" \
                " -b ${PRIMER} -e"
	log_string ""
	log_string "--- Sort trimmed bam"
	log_command "samtools sort  map/${SAMPLE}.trim.bam -o map/${SAMPLE}.trim.sort.bam"
	log_command "samtools index map/${SAMPLE}.trim.sort.bam"
	log_string ""
}

################################################################################
# Variant calling with ivar
################################################################################
ivar_variants () 
{
	SAMPLE=$1

	# ivar SNP calling and consensus pipeline
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Variant calling with ivar"
	log_string "-----------------------------------------------------------------"
	log_command "samtools mpileup -aa -A -d 0 -B -Q 0 map/${SAMPLE}.trim.sort.bam" \
              "--reference ${REF_GENOME} | "                                   \
              "ivar variants -p variants/${SAMPLE} -q ${MINQUAL_VAR}"          \
              " -t ${MINFREQ_VAR} -m ${MINDEPTH_VAR} -r ${REF_GENOME}"         \
              " -g ${ANNOTATION}"
	log_string ""
}

################################################################################
# Consensus calling with ivar
################################################################################
ivar_consensus () 
{
	SAMPLE=$1
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Consensus calling with ivar"
	log_string "-----------------------------------------------------------------"
	log_command "samtools mpileup -aa -A -d 0 -B -Q 0 map/${SAMPLE}.trim.sort.bam |" \
                 "ivar consensus -p consensus/${SAMPLE} -q ${MINQUAL_CONS}"      \
                 "-t ${MINFREQ_CONS} -m ${MINDEPTH_CONS} -n ${AMBIGUOUS_CHAR}"
	log_string ""
	# CLEAN CONSENSUS HEADER
	# Consensus_COV001952_threshold_0.8_quality_20 to COV001952
	sed -e "s/Consensus_//" consensus/${SAMPLE}.fa                    \
	    -e "s/_threshold_${MINFREQ_CONS}_quality_${MINQUAL_CONS}//" > \
		consensus/${SAMPLE}.tmp
	mv consensus/${SAMPLE}.tmp consensus/${SAMPLE}.fa
}

################################################################################
# Statistics and coverage reports
################################################################################
report () 
{
	SAMPLE=$1
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "	Flagstat statistics and coverage calculation"
	log_string "-----------------------------------------------------------------"

	log_string ""
	log_command "samtools flagstat map/$SAMPLE.trim.sort.bam >" \
                "reports/${SAMPLE}.bamstats"
	
	log_string ""
	log_command "samtools depth -aa --reference ${REF_GENOME}" \
                "map/${SAMPLE}.trim.sort.bam > reports/${SAMPLE}.depth"
	
	log_string ""
	log_command "python $SCRIPT_DIR/../utils/COVID-window-coverage.py" \
                "-i reports/${SAMPLE}.depth -p ${PRIMER}"              \
                "-w ${WIND_DEPTH} -H -s ${SAMPLE} > reports/${SAMPLE}.wdepth"
	
	log_string ""
	log_command "python $SCRIPT_DIR/../utils/COVID-amplicon-depth.py" \
                "-i reports/${SAMPLE}.depth -p ${PRIMER}"             \
                "-o reports/${SAMPLE}.adepth -s ${SAMPLE} -r ${RUNID}"
	log_string ""
}

################################################################################
# Clean results directory
################################################################################
clean_files () 
{
	SAMPLE=$1
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Cleaning files"
	log_string "-----------------------------------------------------------------"
	log_command "rm ${SAMPLE}.sort.bam ${SAMPLE}.sort.bam.bai"
	log_command "rm ${SAMPLE}.trim.bam"
	log_command "rm ${SAMPLE}.R[12].trim.fastq.gz"
	log_string ""
}

################################################################################
# Write QC_Summary
################################################################################
summary () 
{
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string " Writting QC Summary"
	log_string "-----------------------------------------------------------------"

	# 1 - Generates the QC report (../reports/QC_summary.'${RUNID}'.csv')
	log_string ""
	log_string "--- Get run analysis summary (only COVs samples)"
	log_command "python ${SCRIPT_DIR}/../utils/COVID-analysis-summary.py" \
		" -m ${MTABLE_REAL} "                   \
		" --run-id ${RUNID} "                   \
		" --run-directory ${OUTFOLDER_REAL}"    \
		" -o reports/QC_summary.${RUNID}.csv"
	log_string ""

	# 2 - Generates the 'all' QC report (../reports/QC_summary.'${RUNID}'.all.csv')		
	log_string ""
	log_string "--- Get run analysis summary (all samples)"
	log_command "python ${SCRIPT_DIR}/../utils/COVID-analysis-summary.py" \
		" -m ${MTABLE_REAL} "                   \
		" --run-id ${RUNID} "                   \
		" --run-directory ${OUTFOLDER_REAL}"    \
		" -all yes "                            \
		" -o reports/QC_summary.${RUNID}.all.csv"
	log_string ""

	# Get the seq place
	qc_file=${OUTFOLDER_REAL}'/reports/QC_summary.'${RUNID}'.csv'
	seqplace=$(tail -n +2 ${qc_file} | cut -f3 -d';' | uniq)

	# Call some updating runid scripts
	# 3 - Generates long QC report (../reports/QC_summary.'${RUNID}'.long.csv')		
	log_string "-----------------------------------------------------------------"
	log_string "		LONG QC report generation "
	log_string "-----------------------------------------------------------------"
  	log_string "--- COVID-update-QC_summaries_LONG.sh"
	log_command "python ${SCRIPT_DIR}/../utils/pipeline-data-update/COVID-update-QC_summaries_LONG.sh ${seqplace} ${RUNID}"
}

################################################################################
# Assign lineages with PANGOLIN
################################################################################
assign_lineages () 
{
	mkdir pangolin
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Lineage assignment with PANGOLIN"
	log_string "-----------------------------------------------------------------"
  	log_string "--- Creating a multifasta for pangolin"
	log_command "cat consensus/*.fa > consensus/consensus.${RUNID}.fna"
	log_command "pangolin consensus/consensus.${RUNID}.fna" \
	 	" -o pangolin/ --outfile lineages.${RUNID}.csv"     \
		" --max-ambig 0.7"		
	log_string ""
	#log_command "rm consensus/consensus.${RUNID}.fna"
}

################################################################################
# Create Metadata
################################################################################
# Create metadata tables ready for GISAID, MICROREACT and ENA submission
create_metadata () 
{
	log_string "-----------------------------------------------------------------"
	log_string "		Generating metadata tables "
	log_string "-----------------------------------------------------------------"	

	# Auxiliar variables
	metadata_folder=${OUTFOLDER_REAL}'/metadata'
	dbupdate_file=${metadata_folder}'/DB_UPDATE_'${RUNID}'.csv'

	# Call the analisys-metadata script
	log_string ""
	log_command "python ${SCRIPT_DIR}/../utils/COVID-analysis-metadata.py " \
		" -m ${MTABLE_REAL} " \
		" --run-id ${RUNID} " \
		" --run-directory ${OUTFOLDER_REAL} " \
		" -o ${metadata_folder} "
	log_string ""


	# Check file dbupdate file was generated
	if [ ! -f ${dbupdate_file} ]; then
		log_string "[ERROR]: DB Updating file was not generated: ${dbupdate_file}"
		return
	fi

	# Get the seq place
	seqplace=$(tail -n +2 ${dbupdate_file} | cut -f7 -d',' | uniq)

	# Call some updating runid scripts
	# 1 - Generates clades report (../variants/Typing_Report.'${RUNID}'.txt')		
	log_string "-----------------------------------------------------------------"
	log_string "		Clades & mutations report generation "
	log_string "-----------------------------------------------------------------"
  	log_string "--- COVID-update-clades.sh"
	log_command "python ${SCRIPT_DIR}/../utils/pipeline-data-update/COVID-update-clades.sh ${seqplace} ${RUNID}"

}

################################################################################
# Concatenated Multifasta
################################################################################
# Create a multifasta with genomes that pass QC threshold of 90% coverage and
#  ready for manual inspection for GISAID upload and
# lineae assinment with pangolin
multifasta_consensus () 
{
	echo "--- Creating HQ multifasta"
	# Read which genomes pass QC 90% from summary
	tail -n +2 reports/QC_summary.${RUNID}.csv  |
	awk -v FS=';' '$6 >= 0.9' | cut -f 1 -d ';' | 
	while read COV
	do
		sed -e "s/Consensus_//" consensus/${COV}.fa \
		    -e "s/_threshold_${MINFREQ_CONS}_quality_${MINQUAL_CONS}//" > \
				consensus/${COV}.tmp
		mv consensus/${COV}.tmp consensus/${COV}.fa
		cat consensus/${COV}.fa
	done > consensus/consensus.${RUNID}.fasta
	
	# Create a consensus with GISAID headers read from metadata
	echo "--- Creating GISAID multifasta with SARS-Cov-2 reference genome"
	cat ${REF_GENOME} > consensus/GISAID_consensus.${RUNID}.fasta
	tail -n +2 metadata/GISAID_${RUNID}_metadata.csv |
	cut -f3,26 -d'@' | sed 's/@/ /' | 
	while read GISAID_ID COV_ID
	do
		cat consensus/${COV_ID}.fa | sed "s|${COV_ID}|${GISAID_ID}|"
	done >> consensus/GISAID_consensus.${RUNID}.fasta

}

################################################################################
# Create a pseudoconsensus
################################################################################
pseudoconsensus () 
{
	# With this pseudoconsensus based on the VCF files all alignments will be the
	# same length and will be suitable to extract SNP alignments with snp-sites
	SAMPLE=$1
	log_string "-----------------------------------------------------------------"
	log_string "--- Creating pseudoconsensus: ${SAMPLE}"
	log_string "-----------------------------------------------------------------"
	log_command "python ${SCRIPT_DIR}/../utils/COVID-pseudo-consensus.py " \
		"-r ${REF_GENOME}"           \
		"-v variants/${SAMPLE}.tsv"  \
		"-d reports/${SAMPLE}.depth" \
		"-o pseudoconsensus/${SAMPLE}.pseudoconsensus.fasta"
}

################################################################################
#                      **********************************
#                                 MAIN SCRIPT
#                      **********************************
################################################################################
# Second block:
#   Arguments provided by user are read, validated and
#   all the pipeline is run with respective function calls

COMMAND_CALL="$0 $*"

################################################################################
# Read input parameters
################################################################################
while getopts ":h:c:m:f:r:o:s:t:k:" option
do
	case "${option}" in
		h)
			usage
			exit 1;;
		c) CONFIG=${OPTARG};;
		m) MTABLE=${OPTARG};;
		f) FF=${OPTARG};;
		o) OUTFOLDER=${OPTARG};;
		r) RUNID=${OPTARG};;
		s) STEPS=${OPTARG};;
		t) THREADS=${OPTARG};;
		k) KRAKEN=${OPTARG};;
		\?)
			usage
			exit 1;;
		:)
			echo "Invalid option: $OPTARG requires an argument" >&2;;
	esac
done
shift "$(($OPTIND -1))"

################################################################################
# Validate input parameters
# IF Valid, change relative paths TO ABSOLUTE PATHS
################################################################################

# Config file
# Master table
if [ -z $CONFIG ]; then
        echo ""
        echo "No CONFIG_FILE provided!" >&2
        echo ""
        usage
        exit 1
else
        CONFIG_FILE_REAL=$(readlink -f ${CONFIG})
        echo "Reading Configuration file: ${CONFIG_FILE_REAL}"
fi


# Master table
if [ -z $MTABLE ]; then
	echo ""
	echo "No MASTER_TABLE table provided!" >&2
	echo ""
	usage
	exit 1
else
	MTABLE_REAL=$(readlink -f ${MTABLE})
	echo "Reading input table: ${MTABLE_REAL}"
fi

# FastQ folder
if [ -z $FF ]; then
	echo ""
	echo "No FASTQ folder provided!" >&2
	echo ""
	usage
	exit 1
else
	FF_REAL=$(readlink -f ${FF})
	echo "Reading input data from ${FF_REAL}/"
fi

# Run Id
if [ -z ${RUNID} ]; then
	echo ""
	echo "No RUNID provided" >&2
	echo ""
	usage
	exit 1
fi

# Output Folder
if [ -z $OUTFOLDER ]; then
	echo ""
	echo "No OUTFOLDER provided" >&2
	echo ""
	usage
	exit 1
else
	OUTFOLDER_REAL=$(readlink -f ${OUTFOLDER})
	echo "Writing results to ${OUTFOLDER_REAL}/"
fi

# Running Threads number
if [ -z $THREADS ]; then
  THREADS=1
else
	case $THREADS in
		[0-9]*) echo "Running $THREADS threads";;
	  *)
		echo "Argument provided for -t (threads) must be numeric"
		exit 1
		;;
	esac
fi


################################################################################
# Get relative paths, date and pipeline parameters
################################################################################

# Get absolute path of the illuminaMapping.sh script
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Init pipeline parameters from pipeline_parameters.config
#source $SCRIPT_DIR/../pipeline_parameters.config
## source $SCRIPT_DIR/../pipeline_parameters_def.config # either change name file or this line
source ${CONFIG_FILE_REAL}

# Init conda environment
source activate ${CONDA_ENV}
if [ $? != 0 ]; then # if there is any problem init conda #!/usr/bin/env
	echo ""
	echo "conda env could not be activated. Please check that a proper conda"\
		"environment is set and that the path to that environment is set in"\
		"variable CONDA_ENV in the configuration file 'pipeline_parameters.config'"
	echo ""
	exit 1
fi

# NOTE THAT AT THIS POINT WE CHANGE TO $OUTFOLDER DIRECTORY AND EVERYTHING
# IS EXECUTED FROM THERE
# Check if OUTFOLDER, was created manually by user with RUNID name previously
# Otherwise, try to create it 
if [[ ! -d "$OUTFOLDER_REAL" ]]; then	
	echo "-----------------------------------------------------------------"
	echo "	Creating ${OUTFOLDER} directory"
	echo "-----------------------------------------------------------------"
	mkdir ${OUTFOLDER}
	OUTFOLDER_REAL=$(readlink -f ${OUTFOLDER})    
fi

echo "-----------------------------------------------------------------"
echo "	Moving to $OUTFOLDER_REAL directory"
echo "-----------------------------------------------------------------"
cd ${OUTFOLDER_REAL}


################################################################################
#                                                          Create directory tree
################################################################################
mkdir consensus
mkdir variants
mkdir logfolder
mkdir reports
mkdir reports/fastqc
mkdir pseudoconsensus
mkdir metadata
mkdir map
mkdir trimm
mkdir kraken

################################################################################
#                                                        Initialize the LOG_FILE
################################################################################

# Get date and define LOG_FILE name
DATE=$(date +"%y%m%d%H%M%S")
LOG_FILE=logfolder/RUN.${RUNID}.log

# Log
log_string "============================================="
log_string "Illumina mapping pipeline"
log_string "please visit: https://github.com/fisabio-ngs/"
log_string "============================================="
log_string "Starting analysis at: $DATE"
log_string "=============== Config viarables ==============="
log_string "CONDA_ENV=${CONDA_ENV}"
log_string "COMMAND_CALL=$COMMAND_CALL"
log_string "----------------------------------------------"
log_string "RUNID=${RUNID}"
log_string "FASTQFOLDER=$FF_REAL"
log_string "OUTFOLDER=$OUTFOLDER_REAL"
log_string "REFERENCE=$REF_GENOME"
log_string "PRIMER=$PRIMER"
log_string ""
log_string "=============== IVAR TRIM VARIABLES ==============="
log_string "MINTRIM_LENGTH=$MINTRIM_LENGTH   # Min read length after primer trimming"
log_string "MINQUALTRIM=$MINQUALTRIM      # Min quality for sliding window"
log_string "WINDOW=$WINDOW            # Window size for quality window trimming"
log_string ""
log_string "=============== IVAR CONSENSUS VARIABLES ==============="
log_string "MINQUAL_CONS=$MINQUAL_CONS    # Min quality for consensus calling"
log_string "MINFREQ_CONS=$MINFREQ_CONS    # Min frequency to consider fixed a SNP in consensus"
log_string "MINDEPTH_CONS=$MINDEPTH_CONS    # Min position depth, ambiguous_char otherwise"
log_string "AMBIGUOUS_CHAR=$AMBIGUOUS_CHAR    # Character to use in consensus for uncovered positions"
log_string ""
log_string "=============== IVAR VARIANT CALLING VARIABLES ==============="
log_string "MINQUAL_VAR=$MINQUAL_VAR      # Min quality for SNP calling"
log_string "MINFREQ_VAR=$MINFREQ_VAR    # Min freq to call a SNP"
log_string "MINDEPTH_VAR=$MINDEPTH_VAR     # Min position depth to call a SNP"
log_string ""
log_string "=============== DEPTH CALCULATION VARIABLES ==============="
log_string "WIND_DEPTH=$WIND_DEPTH      # Window size to compute mean and median depths"
log_string "MIN_DEPTH=$MIN_DEPTH       # Min DEPTH quality threshold (mean)"
log_string "SAMPLES ARRAY=${Samples[@]}"
log_string "Steps="${Steps[@]}
log_string "============================================="
log_string ""


################################################################################
#                                         STORE VARIABLES IN RUN.RUNID.variables
################################################################################
VAR_LOG=logfolder/RUN.${RUNID}.variables
echo "RUNID=${RUNID}" > ${VAR_LOG}
echo "OUTFOLDER_REAL=$OUTFOLDER_REAL" >> ${VAR_LOG}
echo "MTABLE_REAL=$MTABLE_REAL" >> ${VAR_LOG}
echo "MINFREQ_CONS=$MINFREQ_CONS" >> ${VAR_LOG}
echo "MINQUAL_CONS=$MINQUAL_CONS" >> ${VAR_LOG}


################################################################################
#                     Read input data from MASTER_TABLE and execute the pipeline
################################################################################

ivar_version # Print ivar version to be used in the pipeline
fastqc       # Perform initial fastqc

# Cat kraken files to cache to load the DB:
echo "Loading Kraken Database... this process will take some minutes"
#cat ${KRAKEN_DB}database.* > /dev/null

# Kraken filtering
cat ${MTABLE_REAL} | sed 's/;/ /g' | 
while read SAMPLE R1 R2
do
	# Avoid final empty line
	if [ -z ${SAMPLE} ]; then
		continue
	fi

	# Perform the human filtering for such sample
	kraken_filter ${SAMPLE} ${FF_REAL}/${R1} ${FF_REAL}/${R2} ${THREADS} ${KRAKEN}
done

# Full pipeline for each sample
cat ${MTABLE_REAL} | sed 's/;/ /g' | 
while read SAMPLE R1 R2
do

	# Avoid final empty line
	if [ -z ${SAMPLE} ]; then
		continue
	fi

	fastp_trim      ${SAMPLE}
	mapping         ${SAMPLE}
	ivar_trim       ${SAMPLE}
	ivar_variants   ${SAMPLE}
	ivar_consensus  ${SAMPLE}
	report          ${SAMPLE}
	kraken_report   ${SAMPLE}
	pseudoconsensus ${SAMPLE}
done

# Deactivate Conda environment
#conda deactivate 

################################################################################
#                                               LINEAGE ASSIGNMENT WITH PANGOLIN
################################################################################
source activate ${PANG_ENV}
assign_lineages
#conda deactivate 

################################################################################
#                                                         CREATE RESULTS SUMMARY
################################################################################
#summary


################################################################################
#                                                          FINAL QC WITH MULTIQC
################################################################################
source activate ${CONDA_ENV}
log_string ""
log_string "-----------------------------------------------------------------"
log_string "		Run QC assessment with MultiQC"
log_string "-----------------------------------------------------------------"
multiqc . --filename multiqc.${RUNID}.flat.html --flat --outdir reports
multiqc . --filename multiqc.${RUNID}.interactive.html --interactive --outdir reports


################################################################################
#                              CREATE METADATA FILES AND MULTIFASTA GISAID-READY
################################################################################
log_string "-----------------------------------------------------------------"
log_string "		Creating metadata files"
log_string "-----------------------------------------------------------------"
create_metadata
log_string ""
log_string "--- Creating multifasta consensus"
multifasta_consensus


################################################################################
#                                                       CLEAN INTERMEDIATE FILES
################################################################################
cat ${MTABLE_REAL} | sed 's/;/ /g' | 
while read SAMPLE R1 R2
do
	# Avoid final empty line
	if [ -z ${SAMPLE} ]; then
		continue
	fi

	# Remove the sample
#	clean_files ${SAMPLE}
done
conda deactivate


################################################################################
#                                                                  FINAL MESSAGE
################################################################################
log_string ""
log_string "Analysis completed at: ${DATE}"
